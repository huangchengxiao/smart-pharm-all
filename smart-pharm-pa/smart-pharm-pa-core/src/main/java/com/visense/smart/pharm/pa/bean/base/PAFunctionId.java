package com.visense.smart.pharm.pa.bean.base;

///// <summary>
///// 审方中心：处方审核状态
///// </summary>
//public enum PresAuditStatus
//{
//    /// <summary>
//    /// 自动通过
//    /// </summary>
//    AutoPassed = 0,
//    /// <summary>
//    /// 待审核
//    /// </summary>
//    Auditing = 1,
//    /// <summary>
//    /// 审核通过
//    /// </summary>
//    Passed = 2,
//    /// <summary>
//    /// 审核打回
//    /// </summary>
//    Rejected = 3,
//    /// <summary>
//    /// 审核打回，医生接受
//    /// </summary>
//    RejectAccepted = 10,
//    /// <summary>
//    /// 过期自动通过
//    /// </summary>
//    Expired = 4,
//    /// <summary>
//    /// 打回重新提交
//    /// </summary>
//    RejectPending = 8,
//    /// <summary>
//    /// 打回通过
//    /// </summary>
//    RejectPass = 5,
//    /// <summary>
//    /// 打回不通过
//    /// </summary>
//    RejectReject = 6,
//    /// <summary>
//    /// 打印超时
//    /// </summary>
//    PrintExpired = 7,

//    /// <summary>
//    /// 自学习自动通过
//    /// </summary>
//    StudyPassed,

//    /// <summary>
//    /// 自学习自动打回
//    /// </summary>
//    StudyRejected
//}

import com.visense.smart.pharm.pa.bean.ns.INSModule;
import com.visense.smart.pharm.pa.bean.ns.NSModuleBase;
import com.visense.smart.pharm.pa.bean.ns.dbAccess.SpecialModel;
import com.visense.smart.pharm.pa.service.module.SpaceModule;
import com.visense.smart.pharm.pa.service.module.impl.SpaceModuleImpl;
import lombok.Getter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Getter
public enum PAFunctionId
{
	LogIn(1001), // 登录
	LogOut(1002), // 退出
	GetInstruction(1003, SpaceModuleImpl.class), // 说明书
	GetInstructionLink(1004, SpaceModuleImpl.class), // 说明书文件
	GetKeyNote(1005), // 要点提示
	PresAlert(1006), // 处方预警
	PresCheck(1007), // 处方审核
	PresSave(1008), // 处方保存
	PresRevoke(1009), // 作废处方
	GetIssueInfo(1010), // 取问题类型信息
	ClientInfo(1011), // 登录后，返回客户端信息
	GetHospCodes(1012), //登录前,获取所有区域医院代码
	JPCheck(1013), //静配中心审核（处方审核+保存）
	PresCheckSave(1014), //适用于需要处方审核同时保存的情况，例如药房审核等
	DPCheck(1015), //点评分析
	FJInfo(1016), //返回选择的方剂信息
	GetPresAuditStatus(1017), //返回审方中心内处方审核状态
	UpdatePresAuditStatus(1018), //修改审方状态
	RegisterNotice(1019), //挂号通知
	SaveHistoryPres(1020), //提交历史处方
	GetPresAuditStatusEx(1021), //批量返回审方中心内处方审核状态
	GetTemplateInfo(1022), //获取医生回复模板信息
	GetPresAuditResults(1023), //获取处方的分析结果及审核结果
	GetPresAuditResultsEx(1024), //批量查询处方药品完整审核结果
	UpdatePresNo(1025), //批量更新处方号

	RegionInfo(1099), //区域访问信息记录

	//内部使用功能号
	RefreshUserSettings(9001), //重新更新用户设置缓存
	IssueMask(9002), //问题屏蔽(仅限过敏,皮试,药食禁忌提示,按用户屏蔽,客户端操作)
	RuleLevel(9003), //更新问题级别
	GetSettings(9004), //获取设置
	ProblemMask(9005), //问题屏蔽(多个问题屏蔽,主要来源：处方点评)
	GetCanMaskProblem(9006), //获取可以屏蔽的问题类型
	SendMessage(9007); //发送消息

	private int intValue;
	//moduleClass 去容器中拿到这个类,然后执行call
	private Class<? extends NSModuleBase> moduleClass;
	private static Map<Integer, PAFunctionId> mappings = Arrays.stream(PAFunctionId.values()).collect(Collectors.toMap(PAFunctionId::getIntValue, Function.identity()));
	private synchronized static Map<Integer, PAFunctionId> getMappings()
	{
		return Optional.ofNullable(mappings).orElse(new HashMap<>());
	}

	private PAFunctionId(int value)
	{
		intValue = value;
		PAFunctionId.getMappings().put(value, this);
	}
	private PAFunctionId(int value,Class<? extends NSModuleBase> moduleClass)
	{
		intValue = value;
		this.moduleClass = moduleClass;
		PAFunctionId.getMappings().put(value, this);
	}



	public static PAFunctionId forValue(int value)
	{
		return getMappings().get(value);
	}

	public int getValue(){
		return intValue;
	}

}