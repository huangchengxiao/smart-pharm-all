package com.visense.smart.pharm.pa.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.visense.smart.pharm.mybatis.datasource.annotation.DataSource;
import com.visense.smart.pharm.mybatis.enums.DataSourceType;
import com.visense.smart.pharm.pa.domain.DmSysConfField;
import com.visense.smart.pharm.pa.mapper.DemoMapper;
import com.visense.smart.pharm.pa.mapper.klib.Demo2Mapper;
import com.visense.smart.pharm.pa.mapper.pa.PaMapper;
import com.visense.smart.pharm.pa.mapper.ulib.Demo1Mapper;
import com.visense.smart.pharm.pa.service.DemoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class DemoServiceImpl extends ServiceImpl<DemoMapper, DmSysConfField> implements DemoService {
    @Resource
    Demo1Mapper demo1Mapper;
    @Resource
    Demo2Mapper demo2Mapper;
    @Resource
    PaMapper paMapper;


    @DataSource(value = DataSourceType.ULIB)
    @Override
    public String selectHostName() {
        return demo1Mapper.selectHostName();

//        return null;
    }

    @Override
    @DataSource(value = DataSourceType.KLIB)
    public String selectName() {
        return demo2Mapper.selectName();
    }

    @Override
    public String selectPaName() {
        return paMapper.SelectName();
    }
}
