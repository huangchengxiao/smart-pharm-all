package com.visense.smart.pharm.pa.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.visense.smart.pharm.pa.domain.DmSysConfField;

public interface DemoMapper extends BaseMapper<DmSysConfField> {
}
