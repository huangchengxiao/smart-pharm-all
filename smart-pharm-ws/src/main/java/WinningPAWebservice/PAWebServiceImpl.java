package WinningPAWebservice;

import cn.hutool.extra.spring.SpringUtil;
import com.alibaba.fastjson.JSONObject;
import com.visense.smart.pharm.pa.bean.base.PAFunctionId;
import com.visense.smart.pharm.pa.bean.ns.NSModuleBase;
import com.visense.smart.pharm.pa.bean.ns.PAInfo;
import com.visense.smart.pharm.pa.common.PALogging;
import com.visense.smart.pharm.pa.enums.PAErrorCode;
import com.visense.smart.pharm.pa.service.module.SpaceModule;
import com.winning.pa.ws.util.AESUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.xml.ws.Holder;
import java.util.Objects;
import java.util.UUID;

@Component
@Slf4j
public class PAWebServiceImpl implements PAWebService {
    private static long max_span = 0;
    @Resource
    SpaceModule spaceModule;

    @Override
    public int GetPAResults(int funId, String baseData, String detailsData, Holder<String> uiResults, Holder<String> hisResults) {
        System.out.println(funId);
        System.out.println(baseData);
        System.out.println(detailsData);

        //
        if (funId == 0) {
            uiResults.value = "";
            hisResults.value = "";

            return 0;
        }
        long startTime = System.currentTimeMillis();
        String guid = UUID.randomUUID().toString();
        PAFunctionId paFunctionId = PAFunctionId.forValue(funId);
        PAInfo info = new PAInfo(guid, paFunctionId, baseData, detailsData);
        int returnReuslt = 0;
        try {
           /* hisResults.value = "hisResults";
            uiResults.value = AESUtils.encrypt("uiResults", "zhyl4cxrh");*/
            Class<? extends NSModuleBase> moduleClass = paFunctionId.getModuleClass();

            if (Objects.nonNull(moduleClass)) {
                //去容器中获取对应对象
                NSModuleBase moduleBase = SpringUtil.getBean(moduleClass);
                info.init(moduleBase);
                returnReuslt =  moduleBase.Call(info);
                String uiResultData = info.getUIResultData();
                String hisResultData = info.getHISResultData();
                log.info("uiResults:======start");
                log.info(uiResultData);
                log.info("uiResults:======end");
                log.info("hisResults:======start");
                log.info(hisResultData);
                log.info("hisResultData:======end");
                hisResults.value = hisResultData;
                uiResults.value = AESUtils.encrypt( uiResultData,"zhyl4cxrh");
            }
        } catch (Exception e) {
            log.error("ws调用失败==========", e);
            // TODO Auto-generated catch block
            returnReuslt = PAErrorCode.ServiceUnavailable.getValue();
            e.printStackTrace();
            uiResults.value = "";
        }

        long endTime = System.currentTimeMillis();
        long timeElapsed = endTime - startTime;
        if (timeElapsed > max_span) {
            max_span = timeElapsed;
            System.out.println("max_guid = : " + timeElapsed);
        }
        System.out.println("Span in milliseconds: " + timeElapsed);

        PALogging.Info(PAWebService.class, info.getId() + " ︼︼︼︼︼︼︼︼︼︼ GetPAResults LEAVE FUNID = " + funId + "︼︼︼︼︼︼︼︼︼︼" + timeElapsed);
        if (timeElapsed > max_span) {
            max_span = timeElapsed;
            PALogging.Info(PAWebService.class, "max_guid = " + info.getId());
        }
        //FIXME 这个返回待定
        return returnReuslt;
    }

    @Override
    public int GetPAResultsEx(int funId, String baseData, String detailsData, long timeOut, Holder<String> uiResults, Holder<String> hisResults) {
        // TODO Auto-generated method stub
        System.out.println(funId);
        System.out.println(baseData);
        System.out.println(detailsData);

        try {
            uiResults.value = AESUtils.encrypt("uiResults", "zhyl4cxrh");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            uiResults.value = "uiResults";
        }
        hisResults.value = "hisResults";


        return 0;
    }

    @Override
    public String GetAdminAccounts() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String GetClientVersion() {
        // TODO Auto-generated method stub
        return "v20231227";
    }

    @Override
    public int GetPARegionResults(int funId, String baseData, String detailsData, Holder<String> results) {
        // TODO Auto-generated method stub
        return 0;
    }

}
