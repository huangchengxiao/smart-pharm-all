package com.winning.pa.ws;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest(classes = {WsApplication.class})
@ActiveProfiles("dev")
public class WsApplicationTests {


}
