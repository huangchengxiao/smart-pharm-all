package com.winning.pa.ws.ace;

import com.winning.pa.ws.util.AESUtils;

/**
 * @author huangchengxiao
 * @date 2024/3/19 14:04
 */
public class MainTest {
    public static void main(String[] args) {
        String decrypt = AESUtils.decrypt(
                "E0B73BE7B2FAC5B5F38C9A0CA022E882E362DC85DBD46F4934A436B868DD3CD2543C7EAE58677BD4B3B372C28FCA952E7F194FA8F71D0D236B318AB6641B6EF18B8A1C330D92F85F36A8CBF470503D4FE4279F001D71538BBF3E28D063732F928F61B7E241EAD6E8A1B13B20A5CAD5A3CD8E70329AAB521BFB4708C011394F01", "zhyl4cxrh");
        System.out.println();
    }
}
