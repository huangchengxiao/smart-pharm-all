package com.visense.smart.pharm.pa.bean.base;

/** 
 调用PA来源
 
*/
public enum PASource
{
	HIS(0), // 来源：HIS
	YF(1), // 来源：药房
	JPZX(2), // 来源：静配中心
	PA(3), // 来源：处方预处理
	CX(4), // 来源：查询功能
	DP(5), // 来源：处方点评
	CR(6), // 来源：前置审方中心
	NIS(7); // 来源：护士站

	private int intValue;
	private static java.util.HashMap<Integer, PASource> mappings;
	private synchronized static java.util.HashMap<Integer, PASource> getMappings()
	{
		if (mappings == null)
		{
			mappings = new java.util.HashMap<Integer, PASource>();
		}
		return mappings;
	}

	private PASource(int value)
	{
		intValue = value;
		PASource.getMappings().put(value, this);
	}

	public int getValue()
	{
		return intValue;
	}

	public static PASource forValue(int value)
	{
		return getMappings().get(value);
	}
}