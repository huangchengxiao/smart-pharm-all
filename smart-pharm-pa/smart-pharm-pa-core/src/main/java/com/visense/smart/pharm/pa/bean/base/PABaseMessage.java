package com.visense.smart.pharm.pa.bean.base;

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
///#endregion Base Class

import lombok.Data;

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
///#region Base Message
//     从接口传入的基本信息数据，XML格式和JSON格式
//     * <?xml version="1.0" ?>
//     * <base_xml>
//     *      <source>调用来源</source>
//     *      <hosp_code>医院编码</hosp_code>
//     *      <dept_code>科室代码</dept_code>
//     *      <doct>
//     *          <code>医生代码</code>
//     *          <name>医生名称</name>
//     *          <type>医生级别</type>
//     *      </doct>
//     * </base_xml>
//     * 
//     * {
//     *     "Source":"调用来源", "HospCode":"医院编码", "DeptCode":"科室代码", "DeptName":"科室名称"
//     *     "DoctCode":"医生代码", "DoctName":"医生名称", "DoctType":"医生级别" 
//     * }
//
@Data
public class PABaseMessage {
    private PASource source;
    private String hospCode;
    private String deptCode;
    private String deptName;
    private String doctCode;
    private String doctName;
    private String doctType;

}