package com.visense.smart.pharm.pa.enums;

import java.util.HashMap;
import java.util.Optional;

public enum PAErrorCode
{
	Ok(0),
	UnknownFunction(-1), // 未知FUN_ID
	InvalidParameter(-2), // 错误的入参
	DBUnavailable(-3), // 无法连接知识库
	InternalError(-4), // 内部错误
	InvalidModule(-5), // 加载Module失败
	UnauthorizedServer(-6), // 未授权服务器
	ServiceUnavailable(-7), // 无法连接PA服务
	UnknowConfigFunction(-8), // 未知UNKNOWN_CONFIG_FUN
	IncorrectKey(-9), // 错误密钥
	DisabledFunction(-10), //功能未激活

	OverMaxUser(-10001), // 超过客户端最大在线数
	ServiceExpired(-10002), // 超过服务使用期
	OfflineUser(-10003);

	private int intValue;
	private static HashMap<Integer, PAErrorCode> mappings;
	private synchronized static HashMap<Integer, PAErrorCode> getMappings()
	{

		return Optional.ofNullable(mappings).orElse(new HashMap<>());
	}

	private PAErrorCode(int value)
	{
		intValue = value;
		PAErrorCode.getMappings().put(value, this);
	}

	public int getValue()
	{
		return intValue;
	}

	public static PAErrorCode forValue(int value)
	{
		return getMappings().get(value);
	}
}