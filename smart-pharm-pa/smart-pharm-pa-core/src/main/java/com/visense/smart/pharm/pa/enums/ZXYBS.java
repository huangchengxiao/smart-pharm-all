package com.visense.smart.pharm.pa.enums;

import java.util.HashMap;
import java.util.Map;

/**
 中西药标识
 
*/
public enum ZXYBS
{
	Unknown(0),
	Xy(1), // 西药
	Zcy(2), // 中成药
	Cy(3); // 草药

	private int intValue;
	private static java.util.HashMap<Integer, ZXYBS> mappings;
	private synchronized static Map<Integer, ZXYBS> getMappings()
	{
		if (mappings == null)
		{
			mappings = new HashMap<Integer, ZXYBS>();
		}
		return mappings;
	}

	private ZXYBS(int value)
	{
		intValue = value;
		ZXYBS.getMappings().put(value, this);
	}

	public int getValue()
	{
		return intValue;
	}

	public static ZXYBS forValue(int value)
	{
		return getMappings().get(value);
	}
}