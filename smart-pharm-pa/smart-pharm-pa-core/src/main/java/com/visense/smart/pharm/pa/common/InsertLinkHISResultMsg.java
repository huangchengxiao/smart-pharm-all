package com.visense.smart.pharm.pa.common;

import lombok.Data;

@Data
public class InsertLinkHISResultMsg extends PAHISResultMessage
{
	private String fileName;
	private String link;

}