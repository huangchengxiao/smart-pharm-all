package com.visense.smart.pharm.pa.mapper.klib;

import com.baomidou.mybatisplus.core.mapper.Mapper;
import com.visense.smart.pharm.common.core.KeyValue;
import com.visense.smart.pharm.pa.domain.DICInstruction;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author huangchengxiao
 * @date 2024/3/11 9:36
 */
public interface DICInstructionMapper extends Mapper<DICInstruction> {
    @Select("select InstructionName from DIC_Instruction where InstructionID=@InstructionID")
    String getInstructionName(@Param("InstructionID") int InstructionID);
    @Select("select b.DrugName from DIC_Instruction a inner join DIC_DrugBase b on b.DrugID=a.DrugID  where a.InstructionID=@InstructionID")
    List<String> selectDrugName(@Param("InstructionID") Integer InstructionID);

    @Select("select InstructionID key, InstructionName value from DIC_Instruction where DrugID in (select DrugID from DIC_Instruction where " +
            "InstructionID = @InstructionID) and InstructionID <> @InstructionID")
    List<KeyValue> selectInstructionIdAndName(@Param("InstructionID") Integer InstructionID);

    @Select("select PdfFileName from DIC_Instruction where InstructionID=@InstructionID limit 1")
    String getPdfFileName(@Param("InstructionID") int InstructionID);


}
