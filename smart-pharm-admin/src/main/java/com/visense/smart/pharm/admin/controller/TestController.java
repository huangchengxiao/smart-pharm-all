package com.visense.smart.pharm.admin.controller;

import com.visense.smart.pharm.common.pojo.CommonResult;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
@Tag(name = "测试下")
public class TestController {


    @GetMapping("demo")
    @Operation(summary = "测试方法")
    @Parameters({
            @Parameter(name = "name",allowEmptyValue = false,description = "姓名")
    })
    public CommonResult test(String name) {
        return CommonResult.success(String.format("传参为:" + name));
    }
}
