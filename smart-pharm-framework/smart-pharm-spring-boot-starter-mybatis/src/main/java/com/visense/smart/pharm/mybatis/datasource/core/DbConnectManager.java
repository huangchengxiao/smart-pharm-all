package com.visense.smart.pharm.mybatis.datasource.core;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;
import org.sqlite.mc.SQLiteMCRC4Config;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * 功能描述: <br>
 * 〈数据库连接管理〉
 *
 * @Author:wuzhipeng
 * @Date: 2022/6/30 -- 2:16 下午
 */
@Slf4j
public class DbConnectManager {


  public static Connection getSqliteConnection(String url, String password) throws Exception {
    // String jdbcUrl="jdbc:sqlite:file:"+filePath;
    try {
      Connection con;
      con = SQLiteMCRC4Config.getDefault().withKey(password).build().createConnection(url);
      if (con == null) {
        throw new Exception(StrUtil.format("获取sqlite数据库连接失败:url[{}]password[{}]", url, password));
      }
      return con;
    } catch (Exception e) {
      e.printStackTrace();
      throw new Exception(StrUtil.format("获取sqlite数据库连接失败:error[{}]url[{}]password[{}]",
        e.getMessage(), url, password));
    }
  }


  private static Connection getMySqlConnection(String url, String user, String password) throws Exception {
    try {
      Class.forName("com.mysql.jdbc.Driver").newInstance();
      Connection con;
      con = DriverManager.getConnection(url, user, password);
      con.setAutoCommit(true);
      return con;
    } catch (Exception e) {
      e.printStackTrace();
      throw new Exception("获取mysql数据库连接失败" + e.getMessage());
    }
  }

  private static Connection getSqlServerConnection(String url, String user, String password) throws Exception {
    try {
      Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
      Connection conn = DriverManager.getConnection(url, user, password);
      return conn;
    } catch (Exception e) {
      e.printStackTrace();
      throw new Exception("获取sqlserver数据库连接失败" + e.getMessage());
    }
  }

  private static Connection getSqlServer2000Connection(String url, String user, String password) throws Exception {
    try {
      Class.forName("com.microsoft.jdbc.sqlserver.SQLServerDriver").newInstance();
      Connection conn = DriverManager.getConnection(url, user, password);
      return conn;
    } catch (Exception e) {
      e.printStackTrace();
      throw new Exception("获取SqlServer2000数据库连接失败" + e.getMessage());
    }
  }

  private static Connection getOracleConnection(String url, String user, String password) throws Exception {
    try {
      Class.forName("oracle.jdbc.driver.OracleDriver").newInstance();
      Connection con;
      con = DriverManager.getConnection(url, user, password);
      con.setAutoCommit(true);
      return con;
    } catch (Exception e) {
      e.printStackTrace();
      throw new Exception("获取Oracle数据库连接失败" + e.getMessage());
    }
  }


  /**
   * 功能描述: <br>
   * 〈获取数据库连接,使用完毕一定要记得手动释放数据库连接〉
   *
   * @param url
   * @param user
   * @param password
   * @param type     sqlServer、oracle、mysql、sqlServer2000、
   * @return:java.sql.Connection
   * @Author:Juveniless
   * @Date: 2019/5/18 0018 -- 12:43
   */
  public static Connection getDbConnection(String url, String user, String password, String type) throws Exception {
    Connection con = null;
    if ("sqlServer".equalsIgnoreCase(type)) {
      try {
        con = DbConnectManager.getSqlServerConnection(url, user, password);
      } catch (Exception e) {
        throw new Exception(e);
      }
    } else if ("oracle".equalsIgnoreCase(type)) {
      try {
        con = DbConnectManager.getOracleConnection(url, user, password);
      } catch (Exception e) {
        throw new Exception(e);
      }
    } else if ("mysql".equalsIgnoreCase(type)) {
      try {
        con = DbConnectManager.getMySqlConnection(url, user, password);
      } catch (Exception e) {
        throw e;
      }
    } else if ("sqlServer2000".equalsIgnoreCase(type)) {
      try {
        con = DbConnectManager.getSqlServer2000Connection(url, user, password);
      } catch (Exception e) {
        throw e;
      }
    } else if ("sqlite".equalsIgnoreCase(type)) {
      try {
        con = DbConnectManager.getSqliteConnection(url, password);
      } catch (Exception e) {
        throw e;
      }
    } else {
      String msg = "仅支持mysql,sqlServer,oracle,sqlite数据库";
      log.error(msg);
      try {
        throw new Exception(msg);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    return con;
  }

  /**
   * 功能描述: <br>
   * 〈获取数据库连接〉
   *
   * @param dataSourceJsonConfig 数据库连接配置json文件
   * @return:java.sql.Connection
   * @Author:Juveniless
   * @Date: 2019/5/25 0025 -- 16:30
   */
  public static Connection getDbConnection(String dataSourceJsonConfig) throws Exception {
    Connection con = null;
    JSONObject json = null;
    json = JSONUtil.parseObj(dataSourceJsonConfig);
    String type = json.getStr("type").trim();
    String url = json.getStr("url").trim();
    String user = json.getStr("user").trim();
    String password = json.getStr("password").trim();
    con = getDbConnection(url, user, password, type);
    return con;
  }


}
