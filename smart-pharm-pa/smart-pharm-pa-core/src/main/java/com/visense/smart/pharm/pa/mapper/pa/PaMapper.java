package com.visense.smart.pharm.pa.mapper.pa;

import org.apache.ibatis.annotations.Select;

public interface PaMapper {

    @Select("SELECT top 1 name  FROM CR_ClientAllergy")
    String SelectName();
}
