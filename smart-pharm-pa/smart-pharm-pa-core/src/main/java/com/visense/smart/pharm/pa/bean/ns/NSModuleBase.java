package com.visense.smart.pharm.pa.bean.ns;


import com.visense.smart.pharm.pa.bean.base.PAFunctionId;

public abstract class NSModuleBase implements INSModule {

    public abstract PAFunctionId[] GetFunIds();

    public abstract int Call(IPAInfo info);

    public String GetAuthorizeCode() {
        return "";
    }

    public boolean Init() {
        return true;
    }

    public void UnInit() {
        return;
    }

}