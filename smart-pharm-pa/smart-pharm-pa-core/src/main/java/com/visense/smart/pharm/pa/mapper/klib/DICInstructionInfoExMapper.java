package com.visense.smart.pharm.pa.mapper.klib;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @author huangchengxiao
 * @date 2024/3/14 10:55
 */
public interface DICInstructionInfoExMapper {

    @Select("SELECT COUNT(1) from DIC_InstructionInfoEx where InstructionID=#{InstructionID}")
    Integer selectCountByInstructionID(@Param("InstructionID") Integer InstructionID);



}
