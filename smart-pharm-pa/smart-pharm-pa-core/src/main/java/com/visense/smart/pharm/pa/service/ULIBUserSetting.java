package com.visense.smart.pharm.pa.service;


import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.visense.smart.pharm.mybatis.datasource.annotation.DataSource;
import com.visense.smart.pharm.mybatis.enums.DataSourceType;
import com.visense.smart.pharm.pa.domain.ulib.HosInfo;
import com.visense.smart.pharm.pa.mapper.ulib.HosInfoMapper;
import com.visense.smart.pharm.pa.mapper.ulib.SettingMapper;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 * PA配置
 * <p>
 * //public enum ULIBSetting
 * //{
 * //    HOSP_AREA = 0,                          // 医院所在地区
 * //    HOSP_NAME = 1,                          // 医院名称
 * //    HOSP_CODE = 2,                          // 医院代码
 * //    WYYPCYXHZY = 3,                         // 外用药品参与相互作用  RX-001-001
 * //    XTSDACFYYTS = 4,                        // 相同SDA重复用药提示   RX-007-002
 * //    CFCYXHZY = 5,                           // 成分参与相互作用      RX-001-002
 * //    WEST_CHINESE_IS_SPLIT = 6,              // 开具处方中西药、草药拆分设置值 0 不拆分 1 只草药拆分 2 中药、西药、草药都需拆分
 * //    PRES_LIMIT_COUNT = 7,                   // 处方药品品种数限制值 0 不限制
 * //    PRES_DRUGCOUNT_GROUPING = 8,            // 是否把一组配液计算为一种品种
 * //    PRES_DRUG_COUNTTYPE = 9,                // 处方药品品种数计算方式
 * //    GENERALNAME_COUNTTYPE = 10,              // 通用名称计算方式
 * //    KEYPOINT_CY_UNMATCH = 11,                // 是否显示草药未匹配 TT-001-001 （己作废）
 * //    KEYPOINT_ZZ_UNMATCH = 12,                // 是否显示自制与原料药品未匹配  TT-010-001
 * //    KEYPOINT_APPEND_JLYBXX = 13,             // 是否在乙类医保信息后附加甲类信息 TT-002-001
 * //    KEYPOINT_CORDIAL_JCFL = 14,              // 兴奋剂提示时是否检查辅料 TT-005-001
 * //    KEYPOINT_FOOD_JCFL = 15,                 // 辅料是否参与食物与药物相关信息的提示 TT-009-001
 * //    RX_CYGYTJ = 16,                          // 屏蔽草药给药途径审查 RX-006-001
 * //    RX_GYTJSY = 17,                          // 是否屏蔽给药途径慎用 RX-006-002
 * //    RX_LNRYYSY = 18,                         // 是否分析老年人用药慎用 RX-004-001
 * //    RX_ETYYSY = 19,                          // 是否分析儿童用药慎用 RX-004-002
 * //    KEYPOINT_YBYP = 20,                      // 医保药品相关内容提示 TT-002
 * //    KEYPOINT_JBYP = 21,                      // 基本药品相关内容提示 TT-003
 * //    KEYPOINT_MZYP = 22,                      // 麻醉药品相关内容提示 ADU-TT-01-05
 * //    KEYPOINT_JSYP = 23,                      // 精神药品相关内容提示 ADU-TT-01-06
 * //    KEYPOINT_KJYP = 24,                      // 抗菌药品相关内容提示 TT-007
 * //    INSTALL_DRUG_DAYS_MODULE = 25,           // 门、急诊处方量（天数审核） AD-TT-15
 * //    INSTALL_ICD10_CONTRAIN_MODULE = 26,      // ICD10禁忌症 RX-009
 * //    CFG_MZYPCFLSH = 27,                      // 麻醉药品处方量审核 ADU-TT-16
 * //    CFG_JSYPCFLSH = 28,                      // 精神药品处方量审核 ADU-TT-17
 * //    RESULT_NEED_ENCRYPT = 29,                // 是否对前台数据库数据加密 0 不加密， 1 加密
 * //    INSTALL_CONVENTIONAL_USAGE_MODULE = 30,  // 是否启用常规用药量审核 ADU-TT-18
 * //    KEYPOINT_CY = 31,                        // 是否显示草药要点提示 0 不显示， 1 显示， 默认不显示 TT-001-001
 * //    WYYPCFYYTS = 32,                         // 外用药品重复用药提示   RX-007-002
 * //    INSTALL_ICD10_ADAPTATION_MODULE = 33,    // ICD10适应症 RX-009？
 * //    RX_JMPZ_SHOW_ALL = 34,                   // 静配中心问题提示全开开关,是否提示所有情况(禁用，慎用，其它) 0 不提示所有， 1 提示所有， 默认不提示 R-41
 * //    REGION_PATTERN = 35,                     // 区域模式
 * //    CFYYTZTS = 36,                           // 重复用药"同种"提示
 * //    CFYYTLTS = 37,                           // 重复用药"同类"提示
 * //    CFYYTCFTS = 38,                          // 重复用药"同成份"提示
 * //    CFYYXTZLZYTS = 39,                       // 重复用药"相同治疗作用"提示
 * //    INSTALL_DRUG_RIGHT_LIMIT_MODULE = 40,    // 是否启用三线用药审核 ADU-TT-18
 * //    ENABLE_GS_CONTENT = 41,                  // 是否启用GS内容
 * //    INSTALL_HIGH_PRICE_MODULE = 42,          // 高价药模块
 * //    INSTALL_PRESC_SPECIFIC_MODULE = 43,      // 处方规范模块 （药品剂量，规格，数量，单位等书写不规范或不清楚）
 * //    INSTALL_DRUG_TREATMENT_MODULE = 44,             // 门诊，住院疗程模块
 * //    INSTALL_OT_PREVENTIONANTIBIOTICS_MODULE = 45,// 手术预防用抗菌药物使用合理性问题
 * //    INSTALL_ALLERGIC_LABORATORY_MODULE = 46, // 抗菌药品药敏检查相关问题
 * //    INSTALL_DRUG_FREQ_LIMIT_MODULE = 47,       // 药品使用频次审查
 * //    INSTALL_DRUG_FORLIVERANDKIDNEY = 48,       // 肝肾功能异常患者药物使用合理性问题
 * //    ZYXYCYXYWTTS = 49,                       // 屏蔽中药与西药,草药与西药之间问题提示
 * //    PWWTPDCFYDH = 50,                        // 判断配伍问题时是否需要同一处方/医嘱
 * //    ZDYRMWTFX = 51,                          // 是否判断自定义溶媒问题
 * //    ZYZXJRMWTFX = 52,                        // 是否分析中药注射剂溶媒问题
 * //    RX_GMYSY = 53,                           // 是否分析过敏源慎用问题
 * //    RX_RSQSY = 54,                           // 是否分析妊娠期慎用问题
 * //    RX_JJZSY = 55,                           // 是否分析禁忌症慎用问题
 * //    RX_TSRQSY = 56,                          // 是否分析特殊人群慎用问题
 * //    RX_YZYYSY = 57,                          // 是否分析孕周用药慎用问题
 * //    RX_XHZYSY = 58,                          // 是否分析相互作用非重要（慎用）问题
 * //    RX_QTSY = 59,                            // 是否分析其他慎用问题
 * //    ZFXZDYSYZWT = 60,                        // 是否只分析自定义适应症问题
 * //    ENABLE_WEB_RULE_MENU = 61,               // 自定义用户规则是否可用
 * //    FUNCTION_MODULE = 62,                    // 所安装的系统的功能，1：合理用药；2：审方中心；3：药师工作站
 * //    ENABLE_KNOWLEDGE_BASE = 63,               //是否启用智库查询功能
 * //    ENABLE_DP = 100                          //处方点评功能是否启用
 */

//}

public interface ULIBUserSetting {
     int GetUserSetting(int hospCode, String code, int defValue);
    HosInfo GetHospInfo();
}