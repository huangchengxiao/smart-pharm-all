package com.visense.smart.pharm.pa.service.init;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * 这边蛀牙模仿 init 跟destroy
 */
@Service
@Slf4j
public class PaService {
    @Resource
    NSManager nsManager;
    @PostConstruct
    public void init() {
        log.info("进行PaService 的初始化");
        nsManager.Load();
        log.info("完成PaService 的初始化");
    }
}
