package com.visense.smart.pharm.pa.web.controller;

import com.visense.smart.pharm.common.pojo.CommonResult;
import com.visense.smart.pharm.pa.service.module.SpaceModule;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping(name = "spec/module")
@Tag(name = "药品说明书", description = "我是药品说明书相关")
public class SpecModuleController {
    @Resource
    SpaceModule spaceModule;
    @GetMapping("call")
    @Operation(summary = "药品说明书summary", description = "药品说明书描述")
    @Parameter(name = "name", description = "名称")
    public CommonResult call(String name) {
        spaceModule.Call(null);
        return CommonResult.success("call的结果" + name);
    }
}
