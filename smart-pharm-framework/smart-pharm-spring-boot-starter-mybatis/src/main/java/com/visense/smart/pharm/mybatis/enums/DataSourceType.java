package com.visense.smart.pharm.mybatis.enums;

/**
 * 数据源
 * 
 * @author winning
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE,

    /**
     * PADB-知识检索使用
     */
    PADB,
    /**
     * KLIB-知识检索使用
     */
    KLIB,


    ULIB
}
