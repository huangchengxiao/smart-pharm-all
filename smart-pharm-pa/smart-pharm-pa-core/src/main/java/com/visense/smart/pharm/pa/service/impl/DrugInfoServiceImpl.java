package com.visense.smart.pharm.pa.service.impl;

import cn.hutool.core.convert.Convert;
import com.alibaba.druid.DbType;
import com.visense.smart.pharm.mybatis.datasource.annotation.DataSource;
import com.visense.smart.pharm.mybatis.enums.DataSourceType;
import com.visense.smart.pharm.pa.common.PALogging;
import com.visense.smart.pharm.pa.mapper.klib.DICInstructionInfoExMapper;
import com.visense.smart.pharm.pa.mapper.klib.DICInstructionInfoMapper;
import com.visense.smart.pharm.pa.mapper.klib.DICInstructionMapper;
import com.visense.smart.pharm.pa.mapper.pa.HosDrugMapMapper;
import com.visense.smart.pharm.pa.service.DrugInfoService;
import com.visense.smart.pharm.pa.service.init.PAAccessModule;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Optional;

/**
 * @author huangchengxiao
 * @date 2024/3/14 9:23
 */
@Service
public class DrugInfoServiceImpl implements DrugInfoService {
    @Resource
    HosDrugMapMapper drugMapMapper;
    @Resource
    DICInstructionInfoExMapper dicInstructionInfoExMapper;
    @Resource
    DICInstructionMapper dicInstructionMapper;
    @Resource
    DICInstructionInfoMapper dicInstructionInfoMapper;

    public int GetCode(int hospCode, String hisCode, String hospFlag, boolean confirmedOnly) {
        if (hospCode == 0) {
            return 0;
        }
        try {
            return Optional.ofNullable(drugMapMapper.selectDrugCode(hospCode, hisCode, confirmedOnly)).orElse(0);
        } catch (Exception e) {
            return 0;
        }
    }


    /**
     * 获取药品说明书名称
     *
     * @param
     * @return
     */
    @DataSource(DataSourceType.KLIB)
    public String GetInsertName(int code) {
        if (code == 0) {
            return "";
        }
        String name = "";
        try {
            Integer objExist = dicInstructionInfoExMapper.selectCountByInstructionID(code);
            if (objExist != null && objExist > 0) //有结构化说明书
                return code + ".pdf";
            objExist = dicInstructionInfoMapper.selectCountByInstructionID(code);
            if (objExist != null && objExist > 0) //有结构化说明书
                return code + ".pdf";
            String pdfFileName = dicInstructionMapper.getPdfFileName(code);
            if (StringUtils.isNotBlank(pdfFileName)) {
                name = pdfFileName;
            }
        } catch (RuntimeException ex) {

        }
        return name;
    }
}
