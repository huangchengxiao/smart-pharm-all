package com.visense.smart.pharm.pa.mapper.ulib;

import com.visense.smart.pharm.mybatis.core.mapper.RootMapper;
import com.visense.smart.pharm.pa.domain.ulib.HosInfo;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author huangchengxiao
 * @date 2024/3/12 16:47
 */
public interface HosInfoMapper extends RootMapper<HosInfo> {

 /*   default HosInfo selectFirst(){
        return selectList().stream().findFirst().orElse(null);
    };*/
     @Select("SELECT HospCode, HospName, HospArea FROM HosInfo")
     List<HosInfo> selectAll();
}
