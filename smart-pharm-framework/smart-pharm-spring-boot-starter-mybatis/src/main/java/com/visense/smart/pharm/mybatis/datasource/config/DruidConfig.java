package com.visense.smart.pharm.mybatis.datasource.config;

import cn.hutool.extra.spring.EnableSpringUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;
import com.alibaba.druid.spring.boot.autoconfigure.properties.DruidStatProperties;
import com.alibaba.druid.util.Utils;
import com.visense.smart.pharm.mybatis.datasource.DynamicDataSource;
import com.visense.smart.pharm.mybatis.datasource.aspect.DataSourceAspect;
import com.visense.smart.pharm.mybatis.datasource.bean.DruidConfigPre;
import com.visense.smart.pharm.mybatis.datasource.config.properties.DruidProperties;
import com.visense.smart.pharm.mybatis.datasource.core.SqliteDataSource;
import com.visense.smart.pharm.mybatis.enums.DataSourceType;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;

import javax.servlet.*;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * druid 配置多数据源
 *
 * @author winning
 */
@Configuration
@Slf4j
@EnableSpringUtil
@Import(value = {DruidProperties.class,  DataSourceAspect.class})
public class DruidConfig {

    @Bean
    @ConfigurationProperties("spring.datasource.druid.master")
    public DataSource masterDataSource(DruidProperties druidProperties) {
        return getAdapterDataSource(druidProperties, DruidConfigPre.MASTER_NAME);
    }

    @Bean
    @ConfigurationProperties("spring.datasource.druid.slave")
    @ConditionalOnProperty(prefix = "spring.datasource.druid.slave", name = "enabled", havingValue = "true")
    public DataSource slaveDataSource(DruidProperties druidProperties) {
        return getAdapterDataSource(druidProperties, DruidConfigPre.SLAVE_NAME);
    }

    @Bean
    @ConfigurationProperties("spring.datasource.druid.ulib")
    @ConditionalOnProperty(prefix = "spring.datasource.druid.ulib", name = "enabled", havingValue = "true")
    public DataSource ulibDataSource(DruidProperties druidProperties) {
        return getAdapterDataSource(druidProperties, DruidConfigPre.ULIB_NAME);
    }

    @Bean
    @ConfigurationProperties("spring.datasource.druid.klib")
    @ConditionalOnProperty(prefix = "spring.datasource.druid.klib", name = "enabled", havingValue = "true")
    public DataSource klibDataSource(DruidProperties druidProperties) {
        return getAdapterDataSource(druidProperties, DruidConfigPre.KLIB_NAME);
    }

    @Bean(name = "dynamicDataSource")
    @Primary
    public DynamicDataSource dataSource(DataSource masterDataSource) {
        Map<Object, Object> targetDataSources = new HashMap<>();
        targetDataSources.put(DataSourceType.MASTER.name(), masterDataSource);
        setDataSource(targetDataSources, DataSourceType.SLAVE.name(), "slaveDataSource");
        setDataSource(targetDataSources, DataSourceType.KLIB.name(), "klibDataSource");
        setDataSource(targetDataSources, DataSourceType.ULIB.name(), "ulibDataSource");
        //FIXME 这边还需要看 sqlite相关数据库是否需要 klib 跟ulib
        return new DynamicDataSource(masterDataSource, targetDataSources);
    }

    /**
     * 设置数据源
     *
     * @param targetDataSources 备选数据源集合
     * @param sourceName        数据源名称
     * @param beanName          bean名称
     */
    public void setDataSource(Map<Object, Object> targetDataSources, String sourceName, String beanName) {
        try {
            //这个是去容器中找相关对象是否存在
            DataSource dataSource = SpringUtil.getBean(beanName);
            targetDataSources.put(sourceName, dataSource);
        } catch (Exception e) {
        }
    }
    /*
     */

    /**
     * 去除监控页面底部的广告
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Bean
    @ConditionalOnProperty(name = "spring.datasource.druid.statViewServlet.enabled", havingValue = "true")
    public FilterRegistrationBean removeDruidFilterRegistrationBean(DruidStatProperties properties) {
        // 获取web监控页面的参数
        DruidStatProperties.StatViewServlet config = properties.getStatViewServlet();
        // 提取common.js的配置路径
        String pattern = config.getUrlPattern() != null ? config.getUrlPattern() : "/druid/*";
        String commonJsPattern = pattern.replaceAll("\\*", "js/common.js");
        final String filePath = "support/http/resources/js/common.js";
        // 创建filter进行过滤
        Filter filter = new Filter() {
            @Override
            public void init(javax.servlet.FilterConfig filterConfig) throws ServletException {
            }

            @Override
            public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
                    throws IOException, ServletException {
                chain.doFilter(request, response);
                // 重置缓冲区，响应头不会被重置
                response.resetBuffer();
                // 获取common.js
                String text = Utils.readFromResource(filePath);
                // 正则替换banner, 除去底部的广告信息
                text = text.replaceAll("<a.*?banner\"></a><br/>", "");
                text = text.replaceAll("powered.*?shrek.wang</a>", "");
                response.getWriter().write(text);
            }

            @Override
            public void destroy() {
            }
        };
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(filter);
        registrationBean.addUrlPatterns(commonJsPattern);
        return registrationBean;
    }

    private DataSource getAdapterDataSource(DruidProperties druidProperties, String name) {
        /**
         * {@link DruidProperties#type}
         */
        //然后根据前缀获取相关 type
        Integer type = Integer.valueOf(StringUtils.defaultString(SpringUtil.getProperty(DruidConfigPre.getFull(name, "type")), "0"));
        if (Objects.isNull(type) || Objects.equals(type, 0)) {
            DruidDataSource dataSource = DruidDataSourceBuilder.create().build();
            druidProperties.dataSource(dataSource);
            return dataSource;
        } else if (Objects.equals(type, 1)) {
            //如果是sqlite加密
            String url = SpringUtil.getProperty(DruidConfigPre.getFull(name, "url"));
            String password = SpringUtil.getProperty(DruidConfigPre.getFull(name, "password"));
            if (StringUtils.isBlank(url)) {
                throw new RuntimeException(String.format("%s 请先配置url", name));
            }
            if (StringUtils.isBlank(password)) {
                throw new RuntimeException(String.format("%s 请先配置url", name));
            }
            return new SqliteDataSource(name, url, password);
        }
        throw new RuntimeException(String.format("druid 配置 type【%s】未实现相关datasource", type));
    }
}
