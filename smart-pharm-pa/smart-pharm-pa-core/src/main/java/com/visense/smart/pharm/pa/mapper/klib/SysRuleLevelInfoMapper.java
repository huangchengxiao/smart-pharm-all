package com.visense.smart.pharm.pa.mapper.klib;

import com.visense.smart.pharm.mybatis.core.mapper.RootMapper;
import com.visense.smart.pharm.pa.bean.ns.dbAccess.SysRuleLevelInfo;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author huangchengxiao
 * @date 2024/3/12 15:52
 */
public interface SysRuleLevelInfoMapper extends RootMapper<SysRuleLevelInfo> {

}
