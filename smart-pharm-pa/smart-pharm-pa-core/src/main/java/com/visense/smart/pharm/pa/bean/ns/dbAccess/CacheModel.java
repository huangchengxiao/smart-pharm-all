package com.visense.smart.pharm.pa.bean.ns.dbAccess;

import lombok.Data;

import java.util.List;

@Data
public class CacheModel {
    private int ID;
    private int LevelID;
    private String ItemCodeA;
    private String ItemCodeB;
    private int BaseCodeA;
    private int BaseCodeB;
    private int RuleType;
    private String RuleExt;
    private String RuleExt1;
    private int SourceType;
    private List<SpecialModel> Specials;
    private int HospFlag;
    private boolean IsDefault;
    private int HospCode;

}