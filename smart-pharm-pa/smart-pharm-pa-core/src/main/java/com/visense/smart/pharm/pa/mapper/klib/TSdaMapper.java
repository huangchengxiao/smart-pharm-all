package com.visense.smart.pharm.pa.mapper.klib;

import com.visense.smart.pharm.mybatis.core.mapper.RootMapper;
import com.visense.smart.pharm.pa.domain.TSda;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author huangchengxiao
 * @date 2024/3/11 10:31
 */
public interface TSdaMapper extends RootMapper<TSda> {
    List<TSda> selectInfoByInstructionID(@Param("zxybs") String zxybs,@Param("InstructionID") Integer instructionID);
    @Select("select a.fldm from t_fl a,t_sda b where b.ID= #{instructionID} and a.ywzbm = b.mID and a.fldm like '01%'")
    List<String> selectFldmByInstrunctionId(@Param("InstructionID") Integer instructionID);
}
