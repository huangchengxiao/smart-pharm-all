package com.visense.smart.pharm.pa.common;

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
	///#endregion Medicine Insert Messages

import lombok.Data;
import lombok.experimental.Accessors;

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
	///#region Medicine Insert File Messages
//     返回的说明书结果数据，XML格式和JSON格式
//     * <?xml version="1.0" ?>
//     * <ui_results_xml fun_id="1007">
//     *     <dt_name>药品名称</dt_name>
//     *     <alias name="药品别名">
//     *         <!--药品别名节点, 可循环-->
//     *         <name>药品别名</name>
//     *     </alias>
//     *     <similar name="其他同类药品说明书">
//     *         <!--其他同类药品说明书, 可循环-->
//     *         <medicine>
//     *             <name>药品名称</name>
//     *             <dt_code>药品代码</dt_code>
//     *         </medicine>
//     *     </similiar>
//     *     <instruction>
//     *         <!--说明书内容, 可循环-->
//     *         <info default="默认选中0/1">
//     *             <title color="">标题名称</title>
//     *             <content>说明书内容</content>
//     *         </info>
//     *     </instruction>
//     * </ui_results_xml>
//     *
//     * {
//     *    "Id":"1004", "DrugCode":"111", "FileName":"111.pdf", "Link":"127.0.0.1:880/DrugInsertPdf/111"
//     * }
//     
@Data
public class InsertLinkUIResultMsg extends PAUIResultMessage
{
	private String fileName;
	private String link;

}