package com.winning.pa.ws;

import javax.xml.ws.Endpoint;

import org.apache.cxf.Bus;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.winning.pa.ws.interceptor.MyWebServiceServerInterceptor;

import WinningPAWebservice.PAWebService;


@ComponentScan("WinningPAWebservice")
@Configuration
public class PAWebServiceConfig {
	@Bean
    public ServletRegistrationBean wsServlet(){
        return new ServletRegistrationBean(new CXFServlet(), "/*");
    }

    @Autowired
//    private IPAWebService paWebService;
    private PAWebService paWebService;
    
    @Autowired
    private MyWebServiceServerInterceptor myInterceptor;

    @Autowired
    @Qualifier(Bus.DEFAULT_BUS_ID)
    private SpringBus bus;

    @Bean
    public Endpoint endpoint(){
        EndpointImpl endpoint = new EndpointImpl(bus, paWebService);
        endpoint.publish("/pawebservice.asmx");
        endpoint.getInInterceptors().add(myInterceptor);
        return endpoint;
    }
}
