package com.visense.smart.pharm.pa.bean.base;

//C# TO JAVA CONVERTER
	///#endregion Login Messages

import lombok.Data;

//C# TO JAVA CONVERTER T
	///#region Medicine Insert Messages
//     从接口传入的说明书详细信息数据，XML格式和JSON格式
//     * <?xml version="1.0" ?>
//     * <details_xml>
//     *      <hosp_flag>门诊住院标识</hosp_flag>
//     *      <medicine>
//     *          <dt_code>药品代码</dt_code>
//     *          <his_code>HIS药品代码</his_code>
//     *      </medicine>
//     * </details_xml>
//     * 
//     * {
//     *   "HospFlag":"门诊住院标识", "MedCode":"药品代码", "MedHISCode":"HIS药品代码"
//     * }
//
@Data
public class InsertDetailMsg extends PADetailMessage
{
	private String hospFlag;
	private String medCode;
	private String medHISCode;

}