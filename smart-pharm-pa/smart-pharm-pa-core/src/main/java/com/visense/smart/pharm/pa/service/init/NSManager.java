package com.visense.smart.pharm.pa.service.init;


import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateTime;
import cn.hutool.extra.spring.SpringUtil;
import com.visense.smart.pharm.pa.bean.ns.INSModule;
import com.visense.smart.pharm.pa.bean.ns.INSModuleBase;
import com.visense.smart.pharm.pa.bean.ns.PAInfo;
import com.visense.smart.pharm.pa.common.RefObject;
import com.visense.smart.pharm.pa.domain.ulib.HosInfo;
import com.visense.smart.pharm.pa.service.ULIBUserSetting;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class NSManager {
    @Resource
    ULIBUserSetting ulibUserSetting;

    public static HashMap<String, Integer> hospCodeMap = new HashMap<String, Integer>();

//    public static String baseDirectory = AppDomain.CurrentDomain.BaseDirectory;

    //static public PAAccessModule paAccess = new PAAccessModule();

    //    static Dictionary<PAFunctionId, INSModule> NSModuleDictionary = new Dictionary<PAFunctionId, INSModule>();
    static List<INSModule> NSModuleList = new ArrayList<>();//设置为 public标识 用于

    static boolean serviceValid = true; // 服务器是否到期
    static boolean AuthorizePassed = false; //服务授权是否通过
    static boolean isDemo = false; // 演示版
    static int demoCount = 0; // 演示版分析次数
    static DateTime lastDemoTime; // 最后一次演示分析时间
    static int demoAllowedCount = 500; // 每天容许演示分析次数

/*	private static java.util.Date expirationDate = java.util.Date.getMaxValue();
	public static int MedicineCountToCombine = Integer.parseInt(ConfigurationManager.AppSettings["MedicineCountToCombine"]);
	public static int ThreadMedicineCount = Integer.parseInt(ConfigurationManager.AppSettings["ThreadMedicineCount"]);
	public static int MedicineThreadCount = Integer.parseInt(ConfigurationManager.AppSettings["MedicineThreadCount"]);
	public static int ProcessorCount = Integer.parseInt(System.Environment.GetEnvironmentVariable("NUMBER_OF_PROCESSORS"));
	private static final String RegKey = "SOFTWARE\\EZG";*/

    public void Load() {
        //FIXME 这边暂时只先实现 module 跟cache相关内容
        PAAccessModule.init(); //FIXME 初始化数据库 这个不用做,自动加载
        SysCache.InitCache(); //FIXME 初始化 科室 规则
        // 判断医院代码是否合法
        HosInfo dtHospInfo = ulibUserSetting.GetHospInfo();
        String str_hospCode = Optional.ofNullable(dtHospInfo).map(HosInfo::getHospCode).orElse(Strings.EMPTY);
        //FIXME module 相关加载不用管  AddNSModule 看着没用 暂时先不管
        log.info("加载医疗机构代码与医院代码的对应关系开始");
        log.info("相关【File.Exists】内容还未处理,等后面再处理");
        // 从配置文件中读取 医疗结构跟医院的关系,放入 hospCodeMap=>  hospCodeMap[codeFrom]
        String hospCode = SpringUtil.getProperty("pa.hospCode");// 这个是从配置文件中获取到的暂时先写死 后面配置到
//        医疗机构代码与医院代码的对应关系 医疗机构代码1=医院代码1 空格 医疗机构代码2=医院代码2
        List<String> hospCodeList = Arrays.stream(hospCode.split(" ")).map(StringUtils::deleteWhitespace).collect(Collectors.toList());
        for (String pair : hospCodeList) {
            if (StringUtils.isNotBlank(pair)) {
                if (pair.contains("=")) {
                    String[] values = pair.split("=");
                    String codeFrom = values[0].trim(); // 医疗机构代码 = 医院代码
                    int codeTo = Convert.toInt(values[1]);
                    hospCodeMap.put(codeFrom, codeTo);
                }
            }
        }
        log.info("加载医疗机构代码与医院代码的对应关系结束");
      /*  DataTable dtHospInfo = ULIBUserSetting.GetHospInfo();
        string str_hospCode = string.Empty;
        if (dtHospInfo != null && dtHospInfo.Rows != null && dtHospInfo.Rows.Count > 0)
            str_hospCode = dtHospInfo.Rows[0]["HospCode"].ToString();


        // 判断服务器是否授权
        if (!IsValidKeyFile(str_hospCode))
        {
            AuthorizePassed = false;
            PALogging.Error(typeof(NSManager), "服务器授权信息验证失败！");
            return;
        }
        AuthorizePassed = true;

        if (Directory.Exists(baseDirectory + "bin\\module"))
        {
            string[] files = Directory.GetFiles(baseDirectory + "bin\\module", "*.dll");

            foreach (string filename in files)
            {
                try
                {
                    System.Reflection.Assembly module = System.Reflection.Assembly.LoadFrom(filename);
                    INSModule mdl = module.CreateInstance("PA.NativeService.NSModule") as INSModule;
                    if (mdl == null)
                    {
                        throw new Exception("创建PA.NativeService.NSModule失败！");
                    }

                    // 未被授权
                    if (NSManager.IsModuleAuthorized(mdl) == false)
                    {
                        PALogging.Info(typeof(NSManager), filename + "未被授权!");
                        continue;
                    }

                    if (mdl.Init() == false)
                    {
                        PALogging.Error(typeof(NSManager), filename + "初始化失败!");
                        continue;
                    }

                    AddNSModule(mdl);
                }
                catch (Exception e)
                {
                    // 加载失败
                    PALogging.Error(typeof(NSManager), filename + " " + e.Message);
                    continue;
                }
            }
        }

        PALogging.Info(typeof(NSManager), "加载医疗机构代码与医院代码的对应关系开始");
        // 寻找医疗机构代码与医院代码的对应关系
        string hospCodes = ConfigurationManager.AppSettings["HospCodes"];

        bool isFile = false;

        if (File.Exists(hospCodes))
        {
            isFile = true;
        }
        else if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + hospCodes))
        {
            isFile = true;
            hospCodes = AppDomain.CurrentDomain.BaseDirectory + hospCodes;
        }

        if (isFile)
        {
            try
            {
                hospCodes = File.ReadAllText(hospCodes, Encoding.Default).Replace(" ", "").Replace("\r", "").Replace("\n", "").Replace(",", " ")
                .Replace(";", " ");
            }
            catch (Exception ex)
            {
                PALogging.Error(typeof(NSManager), ex.Message);
            }
        }

        // 从配置文件中读取医院代码
        foreach (string pair in hospCodes.Split(
                new string[] { " " }, StringSplitOptions.RemoveEmptyEntries))
        {
            if (pair.Contains("="))
            {
                string[] values = pair.Split('=');

                string codeFrom = values[0].Trim(); // 医疗机构代码 = 医院代码
                int codeTo = 0;
                PALogging.Info(typeof(NSManager), codeFrom + "=" + values[1].Trim());
                if (int.TryParse(values[1].Trim(), out codeTo))
                {
                    hospCodeMap[codeFrom] = codeTo;
                }
            }
        }

        //// 从homh_information中读取医院代码
        //string sql = "SELECT hos_hosp_code, dt_hosp_code FROM homh_information WHERE is_del = 0";
        //DataTable dt = PAAccessModule.PADBAccess.ExecuteQuery(sql);
        //foreach (DataRow row in dt.Rows)
        //{
        //    string hospCode;
        //    if (Convert.IsDBNull(row["hos_hosp_code"]))
        //    {
        //        hospCode = string.Empty;
        //    }
        //    else
        //    {
        //        hospCode = row["hos_hosp_code"].ToString().Trim();
        //    }
        //    PALogging.Info(typeof(NSManager), hospCode + "=" + row["dt_hosp_code"].ToString().Trim());
        //    hospCodeMap[hospCode] = row["dt_hosp_code"].ToString().Trim();
        //}

        PALogging.Info(typeof(NSManager), "加载医疗机构代码与医院代码的对应关系结束");
    }
catch (ConfigurationErrorsException e)
    {
        PALogging.Error(typeof(NSManager), e.Message);
        PALogging.Error(typeof(NSManager), e.InnerException.Message);
        throw;
    }
catch (Exception e)
    {
        PALogging.Error(typeof(NSManager), e.Message);
        throw;
    }
    }

    public static void Unload() {
        for (INSModule module : NSModuleList) {
            module.UnInit();
        }

        NSModuleList.clear();
        NSModuleDictionary.clear();
    }

    public static List<INSModule> GetListNSModule() {
        return NSModuleList;
    }

    public static int GetHospCode(String code) {
        if (hospCodeMap.containsKey(code)) {
            return hospCodeMap.get(code);
        }

        return 0;*/
    }

    public static boolean CheckStatus(int timeOver) {
        return true;
        // 1.�ж������û��ĵ�ǰʱ���Ƿ񳬹�VALID_DATE
       /* if (expirationDate != null && expirationDate.compareTo(java.util.Date.Today) < 0) {
            PALogging.Error(NSManager.class, "��������Ȩ�ѵ��ڣ�");
            serviceValid = false;

            return serviceValid;
        }

        //if (MaxUser == 0)
        //    return serviceValid;

        // 2.�ж�comh_users_online���������û���pulse_time�Ƿ�ʱ����ӣ�������online_state��Ϊ2
        //            try
        //            {
        //                string strFmt = @"UPDATE comh_users_online SET online_state = '2', offline_time = getdate()
        //                                      WHERE online_state = '1' AND (datediff(second, pulse_time, getdate()) > {0})";
        //                string strCmd = string.Format(strFmt, Convert.ToString(timeOver));
        //                PAAccessModule.PADBAccess.ExecuteNonQuery(strCmd);
        //            }
        //            catch (Exception ex)
        //            {
        //                PALogging.Error(typeof(NSManager), ex.Source + ex.Message);
        //            }

        return serviceValid;*/
    }

    public static boolean IsValidKeyFile(String code) {
		/*String encryptedString = null;
		try
		{
			StreamReader sr = new StreamReader(baseDirectory + "bin\\SNKey");
			encryptedString = sr.ReadToEnd();
			sr.Close();
		}
		catch(FileNotFoundException e)
		{
			PALogging.Error(NSManager.class, "δ��װ��Ȩ�ļ���");
			return false;
		}

		String[] lines = encryptedString.split(new String[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
		// ������Ȩ�ļ���ע��Ϣ���Լ����з�
		if (lines[0].startsWith("***"))
		{
			lines[0] = "";
		}
		encryptedString = DotNetToJavaStringHelper.join(null, lines);

		XmlDocument document = new XmlDocument();
		try
		{
			// ������Ȩ�ļ�����ȡ��Ȩ��Ϣ
			String str = PAUtil.Decrypt(encryptedString);
			if (str == null)
			{
				throw new RuntimeException();
			}
			// ������Ȩ��Ϣ
			document.LoadXml(str);
		}
		catch(RuntimeException e2)
		{
			PALogging.Error(NSManager.class, "�޷���ȡ��Ȩ�ļ���");
			return false;
		}
		// ��Ȩ��Ϣ-ҽԺ����
		String hospCode = document.DocumentElement["H"].InnerText;
		// ��Ȩ��Ϣ-����MAC��ַ
		String networkAdapter = document.DocumentElement["N"].InnerText;
		// ��Ȩ��Ϣ-IP��ַ
		String ip = document.DocumentElement["I"].InnerText;
		// ��Ȩ��Ϣ-�������к�
		String mainboard = document.DocumentElement["M"].InnerText;
		// ��Ȩ��Ϣ-ע����
		String key = document.DocumentElement["K"].InnerText;
		// ��Ȩ��Ϣ-ע���ֵ
		String value = document.DocumentElement["V"].InnerText;
		// ��Ȩ��Ϣ-��ʾ��
		String demo = (document.DocumentElement["D"] == null) ? "0" : document.DocumentElement["D"].InnerText;

		if (demo.equals("1"))
		{
			isDemo = true;
			return true;
		}

		String descriptions = null;
		//// �ж��������к��Ƿ�Ϸ�
		//string mainboard_machine = CRMSUtil.GetMainBoardSerialNumber(ref descriptions);
		//if (mainboard_machine.Trim() != mainboard_auth.Trim())
		//{
		//    PALogging.Error(typeof(NSManager), "��������Ȩ��ϢMʧ�ܣ�");
		//    return false;
		//}
		// �ж�����MAC��ַ��IP��ַ�Ƿ�Ϸ�
		RefObject<String> tempRef_descriptions = new RefObject<String>(descriptions);
		String[] networkAdapters = PAUtil.GetNetworkAdapters(tempRef_descriptions).split(new String[] { Environment.NewLine }, StringSplitOptions
		.RemoveEmptyEntries);
		descriptions = tempRef_descriptions.argvalue;

		boolean bValidMac = false;
		boolean bValidIP = false;
		for (String adapter : networkAdapters)
		{
			if (networkAdapter.equals(adapter))
			{
				bValidMac = true;
				bValidIP = false;
				ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_NetworkAdapterConfiguration WHERE MACAddress
				='" + networkAdapter + "' AND IPEnabled = 'true'");

				for (ManagementObject mo : searcher.Get())
				{
					String[] ip_addrs = (String[])((mo["IPAddress"] instanceof String[]) ? mo["IPAddress"] : null);
					for (String addr : ip_addrs)
					{
						bValidIP = true;
						break;
					}
				}
				break;
			}
		}

		if (bValidMac == false)
		{
			PALogging.Error(NSManager.class, "��������Ȩ��Ϣ������֤ʧ�ܣ�");
			return false;
		}

		if (bValidIP == false)
		{
			PALogging.Error(NSManager.class, "��������Ȩ��ϢIP��֤ʧ�ܣ�");
			return false;
		}

		// �ж�ע��������Ƿ�Ϸ�
		boolean bValid = false;
		RegistryKey subKey = Registry.LocalMachine.OpenSubKey(RegKey, true);
		if (subKey != null)
		{
			Object subValue = subKey.GetValue(key);
			if (subValue != null && subValue.toString().equals(value, StringComparison.CurrentCultureIgnoreCase))
			{
				bValid = true;

				String[] subKeyValueNames = subKey.GetValueNames();
				Guid id = Guid.Empty;

				for (String name : subKeyValueNames)
				{
					RefObject<Guid> tempRef_id = new RefObject<Guid>(id);
					boolean tempVar = !name.equals(key, StringComparison.CurrentCultureIgnoreCase) && Guid.TryParse(name, tempRef_id);
						id = tempRef_id.argvalue;
					if (tempVar)
					{
						subKey.DeleteValue(name);
					}
				}
			}
		}

		if (bValid == false)
		{
			PALogging.Error(NSManager.class, "��������Ȩ��Ϣע�����֤ʧ�ܣ�");
			return false;
		}

		// ��Ȩ��Ϣ-������ʱ��
		try
		{
			String date = null;
			try
			{
				date = document.DocumentElement["E"].InnerText;
			}
			catch(RuntimeException e3)
			{

			}

			if (!DotNetToJavaStringHelper.isNullOrEmpty(date))
			{
				expirationDate = java.util.Date.ParseExact(date, "yyyy-MM-dd", null);
				// �Ƿ�����Ҫ�ټ�����ݿ��ʱ�䣬���ݿ�����뱾�صķ�������λ����̨������
				if (expirationDate.compareTo(java.util.Date.Today) < 0)
				{
					PALogging.Error(NSManager.class, "��������Ȩʧ��, �����ѹ��ڣ�");
					return false;
				}
			}
		}
		catch(RuntimeException e4)
		{
			PALogging.Error(NSManager.class, "������Կʧ�ܣ�����ϵ��˾��");
			return false;
		}*/

        return true;
    }

    // �ж�ģ���Ƿ���Ȩ
    public static boolean IsModuleAuthorized(INSModuleBase module) {
		/*String AuthorizeCode = module.GetAuthorizeCode();
		// �ж�����
		// string Hosp_code = CRMSSetting.Singleton.GetUserSetting(connection, string.Empty, CRMS_SETTING.HOSP_CODE.ToString(), "");

		//ULIB�в�������ΪĬ����Ȩ
		if (!DotNetToJavaStringHelper.isNullOrEmpty(AuthorizeCode) && ULIBUserSetting.GetUserSetting(0, AuthorizeCode, 1) == 0)
		{
			PALogging.Debug(NSManager.class, String.format("��������%1$-20s���������δ����Ȩ��", AuthorizeCode));
			return false;
		}*/

        return true;
    }


    private int PA_WEBSRV(PAInfo info, RefObject<String> uiResultData, RefObject<String> hisResultData) {
        return 0;
        /*if (!AuthorizePassed) {
            info.getLogging().Info(NSManager.class, "��������Ȩû��ͨ����");
            return PAErrorCode.UnauthorizedServer.getValue();
        }

        if (isDemo) {
            if (lastDemoTime.Day < new java.util.Date().Day) {
                demoCount = 0;
            }

            if (demoCount == demoAllowedCount) {
                info.getLogging().Info(NSManager.class, "�Ѵ������ʾ����������");
                return PAErrorCode.UnauthorizedServer.getValue();
            }

            demoCount++;
            lastDemoTime = new java.util.Date();
        }

        info.getLogging().Info(NSManager.class, "�����������MANAGER ENTER FUNID = " + info.getFunId().toString() + "�����������");
        info.getLogging().Info(NSManager.class, "BaseData: " + info.getBaseData());
        info.getLogging().Info(NSManager.class, "DetailData: " + info.getDetailData());

        //info.UIResultsXmlDoc = new XmlDocument();
        //info.HISResultsXmlDoc = new XmlDocument();

        //info.UIResultsXmlDoc.LoadXml(string.Format("<ui_results_xml fun_id='{0}'></ui_results_xml>",
        //    info.FunId.ToString()));

        //info.HISResultsXmlDoc.LoadXml(string.Format("<his_results_xml fun_id='{0}'></his_results_xml>",
        //    info.FunId.ToString()));

        int ret;
        try {
            ret = PA_WEBSRV(info);
        } catch (RuntimeException e) {
            // �ڲ�����
            info.getLogging().Error(NSManager.class, e.getMessage());
            if (info.getUIResultMsg() == null) {
                info.setUIResultMsg(new PAUIResultMessage());
            }
            info.getUIResultMsg().setMsg(e.getMessage());
            //info.UIResultsXmlDoc.DocumentElement.InnerXml = e.Message;
            if (info.getHISResultMsg() == null) {
                info.setHISResultMsg(new PAHISResultMessage());
            }
            info.getHISResultMsg().setMsg(e.getMessage());
            //info.HISResultsXmlDoc.DocumentElement.InnerXml = e.Message;
            ret = PAErrorCode.InternalError.getValue();
        }

        if (info.getUIResultMsg() == null) {
            info.setUIResultMsg(new PAUIResultMessage());
        }
        info.getUIResultMsg().setId(info.getFunId());
        uiResultData.argvalue = JsonConvert.SerializeObject(info.getUIResultMsg());

        if (info.getHISResultMsg() == null) {
            info.setHISResultMsg(new PAHISResultMessage());
        }
        hisResultData.argvalue = JsonConvert.SerializeObject(info.getHISResultMsg());

        info.getLogging().Info(NSManager.class, "UIResultData: " + uiResultData.argvalue);
        info.getLogging().Info(NSManager.class, "HISResultData: " + hisResultData.argvalue);

        info.getLogging().Info(NSManager.class, "ret: " + (new Integer(ret)).toString());
        info.getLogging().Info(NSManager.class, "�����������MANAGER LEAVE FUNID = " + info.getFunId().toString() + "�����������");

        return ret;*/
    }

    public final int PA_WEBSRV(PAInfo info) {
        return 0;
       /* if (!AuthorizePassed) {
            info.getLogging().Info(NSManager.class, "��������Ȩû��ͨ����");
            return PAErrorCode.UnauthorizedServer.getValue();
        }

        info.getLogging().Info(NSManager.class, "PA_WEBSRV start");

        INSModule mdl = null;
        if (!((mdl = NSModuleDictionary.get((PAFunctionId) info.getFunId())) != null)) {
            // thisҲ��MdlDictionary��
            info.getLogging().Info(NSManager.class, "δ֪����:" + info.getFunId().toString());
            if (info.getHISResultMsg() == null) {
                info.setHISResultMsg(new PAHISResultMessage());
            }
            info.getHISResultMsg().setMsg("δ֪���" + info.getFunId().toString() + "��");
            return PAErrorCode.UnknownFunction.getValue();
        }

        if (ULIBUserSetting.GetUserSetting(0, "EnableCRCmd", 0) == 1) {
            boolean bContinue = false;
            if (info.getFunId() == PAFunctionId.PresSave || info.getFunId() == PAFunctionId.PresRevoke || info.getFunId() == PAFunctionId
            .GetPresAuditStatus || info.getFunId() == PAFunctionId.GetPresAuditStatusEx) {
                bContinue = true;
            }
            if (!bContinue) {
                if (info.getHISResultMsg() == null) {
                    info.setHISResultMsg(new PAHISResultMessage());
                }
                info.getHISResultMsg().setMsg("δ�������" + info.getFunId().toString() + "��, ԭ��EnableCRCmd Ϊ 1");
                return 0;
            }
        }

        if (ULIBUserSetting.GetUserSetting(0, "ENABLE_DP", 0) == 2) //��1004,1015������Ч, ��������ȫ��Ч
        {
            boolean bContinue = false;
            if (info.getFunId() == PAFunctionId.GetInstructionLink || info.getFunId() == PAFunctionId.DPCheck) {
                bContinue = true;
            }
            if (!bContinue) {
                if (info.getHISResultMsg() == null) {
                    info.setHISResultMsg(new PAHISResultMessage());
                }
                info.getHISResultMsg().setMsg("δ�������" + info.getFunId().toString() + "��, ԭ��ENABLE_DP Ϊ 2");
                return 0;
            }
        }

        if (ULIBUserSetting.GetUserSetting(0, "ENABLE_DP", 1) == 0) //����δ����
        {
            if (info.getFunId() == PAFunctionId.DPCheck) {
                if (info.getHISResultMsg() == null) {
                    info.setHISResultMsg(new PAHISResultMessage());
                }
                info.getHISResultMsg().setMsg("δ�������" + info.getFunId().toString() + "��, ԭ��ENABLE_DP Ϊ 0");
                return 0;
            }
        }

        if (ULIBUserSetting.GetUserSetting(0, "EnablePharmacyCmd", 1) == 0) //ҩ��/PIVAS�������
        {
            if (info.getFunId() == PAFunctionId.PresCheckSave || info.getFunId() == PAFunctionId.JPCheck) {
                if (info.getHISResultMsg() == null) {
                    info.setHISResultMsg(new PAHISResultMessage());
                }
                info.getHISResultMsg().setMsg("δ�������" + info.getFunId().toString() + "��, ԭ��EnablePharmacyCmd Ϊ 0");
                return 0;
            }
        }

        if (ULIBUserSetting.GetUserSetting(0, "ENABLE_PHARMACY_CR", 1) == 0) //��ҩǰ���������
        {
            if (info.getFunId() == PAFunctionId.GetPharPresAuditStatus || info.getFunId() == PAFunctionId.PharPresSave) {
                if (info.getHISResultMsg() == null) {
                    info.setHISResultMsg(new PAHISResultMessage());
                }
                info.getHISResultMsg().setMsg("δ�������" + info.getFunId().toString() + "��, ԭ��ENABLE_PHARMACY_CR Ϊ 0");
                return 0;
            }
        }

        int ret = 0;
        try {
            info.init(mdl);
            ret = mdl.Call(info);
            info.Close();
        } catch (PAException e) {
            info.getLogging().Error(NSManager.class, e.getMessage());
            if (info.getUIResultMsg() == null) {
                info.setUIResultMsg(new PAUIResultMessage());
            }
            info.getUIResultMsg().setMsg(e.getMessage());
            if (info.getHISResultMsg() == null) {
                info.setHISResultMsg(new PAHISResultMessage());
            }
            if (e.getErrorCode() == PAErrorCode.DisabledFunction) {
                info.getHISResultMsg().setMsg("δ�������" + info.getFunId().toString() + "��");
            } else if (e.getErrorCode() == PAErrorCode.InvalidParameter) {
                info.getHISResultMsg().setMsg("�쳣������" + e.getMessage() + "��");
            } else {
                info.getHISResultMsg().setMsg("δ֪���" + info.getFunId().toString() + "��");
            }
            return e.getErrorCode().getValue();
        }

        return ret;*/
    }

    public static String DoctInfo(String DoctCode, int HospCode) {
        /*String DoctName = "";
//C# TO JAVA CONVERTER NOTE: The following 'using' block is replaced by its Java equivalent:
//		using (DbHelper helper = PAAccessModule.PADBAccess.GetDB().Clone())
        DbHelper helper = PAAccessModule.PADBAccess.GetDB().clone();
        try {
            java.util.List<DbParameter> parameters = new List<DbParameter>();
            DbParameter parameter = helper.getProviderFactory().CreateParameter();
            try {
                String sql = "select top 1 Name from Hos_Employee (nolock) where EmpNo=@Code and HospCode=@HospCode and Disabled=0";

                parameters = new List<DbParameter>();
                parameter = helper.getProviderFactory().CreateParameter();
                parameter.ParameterName = "@Code";
                parameter.DbType = DbType.String;
                parameter.setValue(DoctCode);
                parameters.add(parameter);
                parameter = helper.getProviderFactory().CreateParameter();
                parameter.ParameterName = "@HospCode";
                parameter.DbType = DbType.Int32;
                parameter.setValue(HospCode);
                parameters.add(parameter);

                helper.Open();
                DoctName = helper.ExecuteScalar(sql, parameters).toString();
            } catch (RuntimeException e) {
                DoctName = "";
            }
        } finally {
            helper.dispose();
        }
        return DoctName;*/
        return "";
    }

    public static String DeptInfo(String DeptCode, int HospCode) {
       /* String DeptName = "";
//C# TO JAVA CONVERTER NOTE: The following 'using' block is replaced by its Java equivalent:
//		using (DbHelper helper = PAAccessModule.PADBAccess.GetDB().Clone())
        DbHelper helper = PAAccessModule.PADBAccess.GetDB().clone();
        try {
            String sql = "";
            java.util.List<DbParameter> parameters = new List<DbParameter>();
            DbParameter parameter = helper.getProviderFactory().CreateParameter();
            //��Hos_Department���л�ȡ
            try {
                sql = "select top 1 Name from Hos_Department (nolock) where Code=@Code and HospCode=@HospCode and isnull(Disabled,0)=0";

                parameters = new List<DbParameter>();
                parameter = helper.getProviderFactory().CreateParameter();
                parameter.ParameterName = "@Code";
                parameter.DbType = DbType.String;
                parameter.setValue(DeptCode);
                parameters.add(parameter);
                parameter = helper.getProviderFactory().CreateParameter();
                parameter.ParameterName = "@HospCode";
                parameter.DbType = DbType.Int32;
                parameter.setValue(HospCode);
                parameters.add(parameter);

                helper.Open();
                DeptName = helper.ExecuteScalar(sql, parameters).toString();
            } catch (RuntimeException e) {
                DeptName = "";
            }
        } finally {
            helper.dispose();
        }
        return DeptName;*/
        return "";
    }
}