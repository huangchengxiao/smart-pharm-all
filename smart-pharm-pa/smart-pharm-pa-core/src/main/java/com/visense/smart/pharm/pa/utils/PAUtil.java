package com.visense.smart.pharm.pa.utils;


public final class PAUtil {
    /*public static String StripNonValidXMLCharacters(String str) {
		*//*if (StringUtils.isBlank(str))
		{
			return str;
		}
		StrUtil.repl
		return Regex.Replace(str, "[\\u0000-\\u0008\\u000B\\u000C\\u000E-\\u001F\\uD800-\\uDFFF\\uFFFE\\uFFFF]", ""); //"[\\x00-\\x08]|[\\x0B-\\x0C]|[\\x0E-\\x1F]",*//*
        *//*String regex = "[\\u0000-\\u0008\\u000B\\u000C\\u000E-\\u001F\\uD800-\\uDFFF\\uFFFE\\uFFFF]";
        String replacement = "cat";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(str);
        String result = matcher.replaceAll("");*//*
     *//*   return Regex.Replace(str,
        @"[\u0000-\u0008\u000B\u000C\u000E-\u001F\uD800-\uDFFF\uFFFE\uFFFF]", string.Empty);*//*

//        Pattern.compile(@"[\u0000-\u0008\u000B\u000C\u000E-\u001F\uD800-\uDFFF\uFFFE\uFFFF]");
        return "";
    }*/

    /**
     * 过滤无效的XML字符
     * @param xml 原始XML字符串
     * @return 过滤后的XML字符串
     */
    public static String StripNonValidXMLCharacters(String xml) {
        StringBuilder filteredXml = new StringBuilder(xml.length());
        for (int i = 0; i < xml.length(); i++) {
            char currentChar = xml.charAt(i);
            if (isValidXmlCharacter(currentChar)) {
                filteredXml.append(currentChar);
            }
        }
        return filteredXml.toString();
    }

    /**
     * 检查字符是否为有效的XML字符
     * @param c 要检查的字符
     * @return 如果字符有效则返回true，否则返回false
     */
    private static boolean isValidXmlCharacter(char c) {
        return c == 0x9 || c == 0xA || c == 0xD || (c >= 0x20 && c <= 0xD7FF)
                || (c >= 0xE000 && c <= 0xFFFD) || (c >= 0x10000 && c <= 0x10FFFF);
    }


}
