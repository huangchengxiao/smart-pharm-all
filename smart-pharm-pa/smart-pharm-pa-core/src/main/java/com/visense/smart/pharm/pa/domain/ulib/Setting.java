package com.visense.smart.pharm.pa.domain.ulib;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author huangchengxiao
 * @date 2024/3/12 17:03
 */
@Data
@TableName("Setting")
public class Setting {
    private Integer HospCode;
    private String FuncCode;
    private String FuncName;
    private Integer Value;
    private String Remark;
}
