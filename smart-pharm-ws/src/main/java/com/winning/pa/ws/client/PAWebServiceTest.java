package com.winning.pa.ws.client;

import javax.xml.ws.Holder;

import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

import WinningPAWebservice.PAWebService;

public class PAWebServiceTest {

	public static void main(String[] args) {
		 JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
		// factory.setAddress("http://172.16.0.109:9090/paWebService.asmx");
		 factory.setAddress("http://localhost:8080/pawebservice.asmx");
		
//		 factory.setAddress("http://172.16.0.165:820/pawebservice.asmx");
		
		 factory.setServiceClass(PAWebService.class);
		 Object o = factory.create();
		 PAWebService service = (PAWebService)o;
		
		 Holder<String> r1 = new Holder<String>();
		 Holder<String> r2 = new Holder<String>();
		
		 int ret = service.GetPAResultsEx(1001, "b-data", "d-data", 1000, r1,
		 r2);
		 System.out.println("返回数据:" + r1.value);
		 System.out.println("返回数据:" + r2.value);
		 System.out.println("返回数据:" + ret);

		// try {
//		Path directory = Paths.get("F:\\已推送（PDF文件）"); // 替换为你的目录路径
//
//		try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(directory)) {
//			for (Path path : directoryStream) {
//				
//				String filePath = path.toString();
//				System.out.println("=== Begin === " + filePath);
//				
//				File file = new File(filePath);
//				PDDocument document = PDDocument.load(file);
//
//				PDDocumentInformation metadata = document.getDocumentInformation();
//				metadata.setTitle(file.getName());
//				document.save("F:\\已推送（PDF文件）-1\\" + file.getName());
//
//				document.close();
//				System.out.println("=== End === " + filePath);
//
//			}
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		
	}

}
