package com.visense.smart.pharm.pa.common;


import lombok.Data;

@Data
public class PAUIResultMessage extends PAResultMessage {
    private int id;
    private int result;
    private String msg;
    private DrugPassResult privateDrugResult;

}