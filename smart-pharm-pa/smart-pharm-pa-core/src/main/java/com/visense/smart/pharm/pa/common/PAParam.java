package com.visense.smart.pharm.pa.common;

import lombok.Data;

import java.io.Serializable;
@Data
public class PAParam implements Serializable
{
	public String guid;
	public int funID;
	public String baseData;
	public String detailData;
	public String uiResultData;
	public String hisResultData;
	public int ret;
}