package com.visense.smart.pharm.admin.config;

import com.visense.smart.pharm.web.swagger.config.PharmSwaggerAutoConfiguration;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 
 *
 * @author huangchengxiao
 */
@Configuration(proxyBeanMethods = false)
public class SwaggerConfiguration {

    /**
     * crm 模块的 API 分组
     */
    @Bean
    public GroupedOpenApi crmGroupedOpenApi() {
        return PharmSwaggerAutoConfiguration.buildGroupedOpenApi("admin","");
    }

}
