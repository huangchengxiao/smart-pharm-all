package com.visense.smart.pharm.pa.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@Slf4j
//TODO 暂时使用包扫描,否则smart-pharm-pa-core的包无法扫描到
@ComponentScan("com.visense.smart.pharm.pa")
public class PaApplication {
    public static void main(String[] args) {
        log.info("Starting PaApplication!!!!!!!!!!!!!!!!!!!!!!!!");
        SpringApplication.run(PaApplication.class, args);
        log.info("Ending PaApplication!!!!!!!!!!!!!!!!!!!!!!!!");
    }
}
