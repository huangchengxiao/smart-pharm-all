package com.visense.smart.pharm.pa.domain.ulib;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author huangchengxiao
 * @date 2024/3/12 16:50
 */
@Data
@TableName("HosInfo")
public class HosInfo {
    private String hospCode;
    private String hospName;
    private String hospArea;
}
