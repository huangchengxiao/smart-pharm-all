package com.visense.smart.pharm.pa.mapper.pa;

import org.apache.ibatis.annotations.Param;

/**
 * @author huangchengxiao
 * @date 2024/3/14 10:18
 */
public interface HosDrugMapMapper {
    Integer selectDrugCode(@Param("hospCode") Integer hospCode,@Param("hosDrugCode") String hosDrugCode,@Param("confirmedOnly") boolean confirmedOnly);
}
