package com.visense.smart.pharm.pa.mapper.ulib;

import org.apache.ibatis.annotations.Select;

public interface Demo1Mapper {
    @Select("select HospName FROM HosInfo limit 1;")
    String selectHostName();
}
