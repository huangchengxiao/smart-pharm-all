package com.visense.smart.pharm.pa.bean.ns;


import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.visense.smart.pharm.pa.bean.base.PABaseMessage;
import com.visense.smart.pharm.pa.bean.base.PADetailMessage;
import com.visense.smart.pharm.pa.bean.base.PAFunctionId;
import com.visense.smart.pharm.pa.common.IPALogging;
import com.visense.smart.pharm.pa.common.NSLogging;
import com.visense.smart.pharm.pa.common.PAHISResultMessage;
import com.visense.smart.pharm.pa.common.PAUIResultMessage;
import com.visense.smart.pharm.pa.service.init.NSManager;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

@Data
public class PAInfo extends IPAInfo {
    public PAInfo() // 用于序列化
    {

    }

    public PAInfo(String id, PAFunctionId funId, String baseData, String detailData) {
        Logging = new NSLogging(id);
        FunId = funId;
        BaseData = baseData;
        DetailData = detailData;
        //BaseXmlDoc = new XmlDocument();
        //DetailsXmlDoc = new XmlDocument();
        //Source = PASource.HIS;
        Id = id;
        //RegionRx = false;
    }

/*	@Override
	public void setHISResultData(String HISResultData) {
		super.setHISResultData(HISResultData);
	}*/

    public void setHISResultMsg(PAHISResultMessage HISResultMsg) {
        this.HISResultMsg = HISResultMsg;
        if (Objects.nonNull(HISResultMsg)) {
            this.HISResultData = JSONObject.toJSONString(HISResultMsg);
        }
    }

    public void setUIResultMsg(PAUIResultMessage UIResultMsg) {
        this.UIResultMsg = UIResultMsg;
        if (Objects.nonNull(UIResultMsg)) {
            this.UIResultData = JSONObject.toJSONString(UIResultMsg);
        }
    }

    public void init(INSModule module) {
        if (BaseData.contains("<?xml")) {
			/*BaseMsg = new PABaseMessage();

			XmlDocument doc = new XmlDocument();
			doc.LoadXml(PAUtil.StripNonValidXMLCharacters(BaseData));

			XmlElement ele = doc.SelectSingleNode("base_xml/hosp_code") as XmlElement;
			if (ele != null)
			{
				BaseMsg.HospCode = ele.InnerText;
			}

			ele = doc.SelectSingleNode("base_xml/dept_code") as XmlElement;
			if (ele != null)
			{
				BaseMsg.DeptCode = ele.InnerText;
			}

			ele = doc.SelectSingleNode("base_xml/dept_name") as XmlElement;
			if (ele != null)
			{
				BaseMsg.DeptName = ele.InnerText;
			}

			ele = doc.SelectSingleNode("base_xml/doct/code") as XmlElement;
			if (ele != null)
			{
				BaseMsg.DoctCode = ele.InnerText;
			}

			ele = doc.SelectSingleNode("base_xml/doct/name") as XmlElement;
			if (ele != null)
			{
				BaseMsg.DoctName = ele.InnerText;
			}

			ele = doc.SelectSingleNode("base_xml/doct/type") as XmlElement;
			if (ele != null)
			{
				BaseMsg.DoctType = ele.InnerText;
			}

			ele = doc.SelectSingleNode("base_xml/source") as XmlElement;
			if (ele == null || ele.InnerText.Length == 0)
			{
				BaseMsg.Source = PASource.HIS;
			}
			else
			{
				switch (ele.InnerText.ToUpper())
				{
					case "HIS":
						BaseMsg.Source = PASource.HIS;
						break;
					case "JPZX":
						BaseMsg.Source = PASource.JPZX;
						break;
					case "YF":
						BaseMsg.Source = PASource.YF;
						break;
					case "PA":
						BaseMsg.Source = PASource.PA;
						break;
					case "CX":
						BaseMsg.Source = PASource.CX;
						break;
					case "NIS":
						BaseMsg.Source = PASource.NIS;
						break;
					default:
						Logging.Error(typeof(PAInfo), "source【" + ele.InnerText.ToUpper() + "】无效");
						break;
				}
			}

			//DetailsXmlDoc = new XmlDocument();
			//DetailsXmlDoc.LoadXml(PAUtil.StripNonValidXMLCharacters(DetailData));
			//PALogging.Info(typeof(PAInfo), DetailsXmlDoc.OuterXml);*/
        } else {
//			BaseMsg = JsonConvert.DeserializeObject<PABaseMessage>(BaseData);
            BaseMsg = JSONObject.parseObject(BaseData, PABaseMessage.class);
        }

        if (BaseMsg == null || StringUtils.isEmpty(BaseMsg.getHospCode()))
            return;

        // 将医疗机构代码转换为医院代码
        if (NSManager.hospCodeMap.containsKey(BaseMsg.getHospCode())) {
            HospCode = NSManager.hospCodeMap.get(BaseMsg.getHospCode());

            if (StringUtils.isEmpty(BaseMsg.getHospCode())) {
                BaseMsg.setHospCode(StrUtil.toString(HospCode));
            }
        } else {
            HospCode = Convert.toInt(BaseMsg.getHospCode());
        }

        OrigHospCode = Convert.toInt(BaseMsg.getHospCode());

        Logging.Info(PAInfo.class, "OrigHospCode: " + OrigHospCode);
        Logging.Info(PAInfo.class, "HospCode: " + HospCode);

        if (!StringUtils.isEmpty(BaseMsg.getDoctCode()) && StringUtils.isEmpty(BaseMsg.getDoctName())) {
            BaseMsg.setDoctName(NSManager.DoctInfo(BaseMsg.getDoctCode(), Convert.toInt(BaseMsg.getHospCode())));
        }

        if (!StringUtils.isEmpty(BaseMsg.getDeptCode()) && StringUtils.isEmpty(BaseMsg.getDeptName())) {
            BaseMsg.setDeptName(NSManager.DeptInfo(BaseMsg.getDeptCode(), Convert.toInt(BaseMsg.getHospCode())));
        }

        //RegionRx = false;

        //            if (BaseMsg.Source == PASource.HIS) // 只有MR功能需要
        //            {
        //                // 判断是否需要跨院处方功能
        //                object obj = PAAccessModule.PADBAccess.ExecuteScalar(@"SELECT sys_value FROM comh_syslang
        //WHERE sys_code = 'SYS003' and sys_is_use = 1 and is_del = 0");

        //                if (obj != null && Convert.ToInt32(obj) > 0)
        //                {
        //                    RegionRx = true;
        //                }
        //            }
    }

    public void Dispose() {
    }

    public void Close() {
    }

    public PAFunctionId FunId;

    public PABaseMessage BaseMsg;

    public PADetailMessage DetailMsg;

    public PAUIResultMessage UIResultMsg;

    public PAHISResultMessage HISResultMsg;

    //public XmlDocument BaseXmlDoc
    //{
    //    get;
    //    set;
    //}

    //public XmlDocument DetailsXmlDoc
    //{
    //    get;
    //    set;
    //}

    //public XmlDocument UIResultsXmlDoc
    //{
    //    get;
    //    set;
    //}

    //public XmlDocument HISResultsXmlDoc
    //{
    //    get;
    //    set;
    //}

    //public String HospCode
    //{
    //    get;
    //    set;
    //}

    public int OrigHospCode;

    public int HospCode;


    public String BaseData;


    public String DetailData;


    public IPALogging Logging;

    public NSManager NSManager;

    public String ClientIp;

    public String ClientMachineName;

    public String Id;

    //public bool RegionRx
    //{
    //    get;
    //    set;
    //} // 是否调用跨院处方功能

    public PAInfo getInfo() {
        return this;
    }

    //public System.Xml.Schema.XmlSchema GetSchema()
    //{
    //    return null;
    //}

    //public void ReadXml(XmlReader reader)
    //{
    //    String val = reader.ReadElementString("FunId");
    //    FunId = Convert.ToUInt32(val);

    //    BaseMsg.HospCode = reader.ReadElementString("HospCode");
    //    DtHospCode = reader.ReadElementString("DtHospCode");
    //    BaseMsg.DeptCode = reader.ReadElementString("DeptCode");
    //    BaseMsg.DoctCode = reader.ReadElementString("DoctCode");
    //    BaseMsg.DoctType = reader.ReadElementString("DoctType");
    //    BaseMsg.DoctName = reader.ReadElementString("DoctName");

    //    val = reader.ReadElementString("Source");
    //    Source = (PASource)Enum.Parse(typeof(PASource), val);

    //    BaseData = reader.ReadElementString("BaseXML");
    //    DetailData = reader.ReadElementString("DetailXML");

    //    val = reader.ReadElementString("UIResultXML");
    //    UIResultsXmlDoc = new XmlDocument();
    //    UIResultsXmlDoc.LoadXml(val);

    //    ClientIp = reader.ReadElementString("ClientIp");
    //    ClientMachineName = reader.ReadElementString("ClientMachineName");

    //    Id = reader.ReadElementString("Id");
    //    Logging = new ModuleLogging(Id);

    //    val = reader.ReadElementString("RegionRx");
    //    RegionRx = Convert.ToBoolean(val);
    //}

    //public void WriteXml(XmlWriter writer)
    //{
    //    writer.WriteElementString("FunId", FunId.ToString());
    //    writer.WriteElementString("HospCode", BaseMsg.HospCode);
    //    writer.WriteElementString("DtHospCode", DtHospCode);
    //    writer.WriteElementString("DeptCode", BaseMsg.DeptCode);
    //    writer.WriteElementString("DoctCode", BaseMsg.DoctCode);
    //    writer.WriteElementString("DoctType", BaseMsg.DoctType);
    //    writer.WriteElementString("DoctName", BaseMsg.DoctName);
    //    writer.WriteElementString("Source", Source.ToString());
    //    writer.WriteStartElement("BaseXML");
    //    writer.WriteCData(BaseData);
    //    writer.WriteEndElement();
    //    writer.WriteStartElement("DetailXML");
    //    writer.WriteCData(DetailData);
    //    writer.WriteEndElement();
    //    writer.WriteStartElement("UIResultXML");
    //    writer.WriteCData(UIResultData);
    //    writer.WriteEndElement();
    //    writer.WriteElementString("ClientIp", ClientIp);
    //    writer.WriteElementString("ClientMachineName", ClientMachineName);
    //    writer.WriteElementString("Id", Id);
    //    writer.WriteElementString("RegionRx", RegionRx.ToString());
    //}
}
