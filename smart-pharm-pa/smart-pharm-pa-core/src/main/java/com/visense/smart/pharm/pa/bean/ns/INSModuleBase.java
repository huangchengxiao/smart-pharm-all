package com.visense.smart.pharm.pa.bean.ns;

public interface INSModuleBase
{
	/** 
	 程序集是否被授权
	 
	*/
	String GetAuthorizeCode();
	boolean Init();
	void UnInit();
}