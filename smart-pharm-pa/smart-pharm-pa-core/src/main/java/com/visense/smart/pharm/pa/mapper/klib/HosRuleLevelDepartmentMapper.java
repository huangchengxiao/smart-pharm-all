package com.visense.smart.pharm.pa.mapper.klib;

import com.visense.smart.pharm.mybatis.core.mapper.RootMapper;
import com.visense.smart.pharm.pa.domain.klib.HosRuleLevelDepartment;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author huangchengxiao
 * @date 2024/3/12 15:30
 */
public interface HosRuleLevelDepartmentMapper extends RootMapper<HosRuleLevelDepartment> {
    @Select("SELECT DepartmentCode,LevelInfoID,ID as RuleID,LevelID,HospCode,HospFlag FROM Hos_RuleLevelDepartment (nolock)  where Deleted=0 and LevelID!=-1 GROUP BY DepartmentCode,LevelInfoID,ID,LevelID,HospCode,HospFlag")
    List<HosRuleLevelDepartment> selectCache();
}
