package WinningPAWebservice;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.ws.Holder;

@WebService(name="PAWebService", targetNamespace = "WinningPAWebservice")
public interface PAWebService {

	@WebMethod(operationName ="GetPAResults", action="http://localhost:8080/GetPAResults")
	int GetPAResults(
			@WebParam(name = "funId", targetNamespace = "WinningPAWebservice") int funId, 
			@WebParam(name = "baseData", targetNamespace = "WinningPAWebservice") String baseData, 
			@WebParam(name = "detailsData", targetNamespace = "WinningPAWebservice") String detailsData,
			@WebParam(name = "uiResults", targetNamespace = "WinningPAWebservice", mode = WebParam.Mode.OUT) Holder<String> uiResults,
			@WebParam(name = "hisResults", targetNamespace = "WinningPAWebservice", mode = WebParam.Mode.OUT) Holder<String> hisResults);
	
	@WebMethod
	public int GetPAResultsEx(
			@WebParam(name = "funId", targetNamespace = "WinningPAWebservice") int funId,
			@WebParam(name = "baseData", targetNamespace = "WinningPAWebservice") String baseData,
			@WebParam(name = "detailsData", targetNamespace = "WinningPAWebservice") String detailsData,
			@WebParam(name = "timeOut", targetNamespace = "WinningPAWebservice") long timeOut,
			@WebParam(name = "uiResults", targetNamespace = "WinningPAWebservice", mode = WebParam.Mode.OUT) Holder<String> uiResults,
			@WebParam(name = "hisResults", targetNamespace = "WinningPAWebservice", mode = WebParam.Mode.OUT) Holder<String> hisResults
		);
	
	@WebMethod
	public String GetAdminAccounts();
	
	@WebMethod
	public String GetClientVersion();

	@WebMethod
	public int GetPARegionResults(
		@WebParam(name = "funId") int funId,
		@WebParam(name = "baseData") String baseData,
		@WebParam(name = "detailsData") String detailsData,
		@WebParam(name = "results", mode = WebParam.Mode.OUT) Holder<String> results
	);
}
