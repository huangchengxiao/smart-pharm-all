package com.visense.smart.pharm.common.core;

/**
 * 可生成 Int 数组的接口
 *
 * @author huangchengxiao
 */
public interface IntArrayValuable {

    /**
     * @return int 数组
     */
    int[] array();

}
