package com.visense.smart.pharm.pa.bean.ns.dbAccess;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//[DBTable("SysRuleLevelInfo")]
@TableName("Sys_RuleLevelInfo")
@Data
public class SysRuleLevelInfo extends DAL_T<SysRuleLevelInfo>
{
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
	//[DBColumn("ID", EnumDBColumnUsage.PrimaryKey | EnumDBColumnUsage.BySystem)]
	@TableId(type = IdType.AUTO)
	public int ID;

//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
	//[DBColumn("LevelID")]
	public int LevelID;

//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
	//[DBColumn("ItemCodeA")]
	public String ItemCodeA;

//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
	//[DBColumn("ItemNameA")]
	public String ItemNameA;

//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
	//[DBColumn("BaseCodeA")]
	public int BaseCodeA;

//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
	//[DBColumn("ItemCodeB")]
	public String ItemCodeB;

//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
	//[DBColumn("ItemNameB")]
	public String ItemNameB;

//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
	//[DBColumn("BaseCodeB")]
	public int BaseCodeB;

//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
	//[DBColumn("RuleType")]
	public int RuleType;

//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
	//[DBColumn("RuleExt")]
	public String RuleExt;

//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
	//[DBColumn("RuleExt1")]
	public String RuleExt1;

//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
	//[DBColumn("SourceType")]
	public int SourceType;

//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
	//[DBColumn("CreatedTime")]
	public Date CreatedTime ;

//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
	//[DBColumn("UpdatedTime")]
	public Date UpdatedTime;
}