package com.visense.smart.pharm.pa.mapper.ulib;

import com.visense.smart.pharm.mybatis.core.mapper.RootMapper;
import com.visense.smart.pharm.mybatis.core.query.LambdaQueryWrapperX;
import com.visense.smart.pharm.pa.domain.ulib.Setting;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author huangchengxiao
 * @date 2024/3/12 16:56
 */
public interface SettingMapper extends RootMapper<Setting>{
     default List<Integer> selectValueByHospCodeAndFuncCode(int HospCode,String FuncCode){
          return selectList(new LambdaQueryWrapperX<Setting>().eq(Setting::getFuncCode,FuncCode).eq(!Objects.equals(HospCode,0),Setting::getHospCode,
                   HospCode)).stream().map(Setting::getValue).collect(Collectors.toList());
     };

}
