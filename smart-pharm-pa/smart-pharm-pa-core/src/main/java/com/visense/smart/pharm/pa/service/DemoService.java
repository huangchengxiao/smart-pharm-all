package com.visense.smart.pharm.pa.service;


import com.visense.smart.pharm.pa.domain.DmSysConfField;

public interface
DemoService {

    String selectHostName();

    String selectName();

    String selectPaName();

}
