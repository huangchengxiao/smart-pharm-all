package com.visense.smart.pharm.pa.bean.ns.dbAccess;

import lombok.Data;

import java.util.List;
@Data
public class SpecialModel
{
	private String privateDeptCode;

	private List<Integer> privateHospFlag;

	private int privateHospCode;

	private int privateLevel;

	private int privateID;

}