package com.visense.smart.pharm.admin.controller;

import com.visense.smart.pharm.common.pojo.CommonResult;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test1")
@Tag(name = "测试下1")
public class TestController1 {


    @GetMapping("demo1")
    @Operation(summary = "测试方法")
    public CommonResult test(String name) {
        return CommonResult.success("传参为:" + name);
    }
}
