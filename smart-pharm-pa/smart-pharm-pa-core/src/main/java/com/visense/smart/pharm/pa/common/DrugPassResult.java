package com.visense.smart.pharm.pa.common;

import lombok.Data;

import java.util.List;

@Data
public class DrugPassResult {
    private String privateUserName;
    private List<String> privatePassDrugs;

    private List<String> privateReturnDrugs;

    private List<String> privateTwiceReturnDrugs;

}