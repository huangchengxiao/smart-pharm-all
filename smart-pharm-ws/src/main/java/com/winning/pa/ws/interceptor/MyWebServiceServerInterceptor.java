package com.winning.pa.ws.interceptor;


import java.util.ArrayList;
import java.util.TreeMap;

import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.springframework.stereotype.Component;

@Component
public class MyWebServiceServerInterceptor extends AbstractPhaseInterceptor<SoapMessage> {
	public MyWebServiceServerInterceptor()
	{
		super(Phase.RECEIVE);
	}
	
	@Override
	public void handleMessage(SoapMessage message) throws Fault {
		TreeMap headers = (TreeMap)message.get(Message.PROTOCOL_HEADERS);
		ArrayList<String> soapAction = new ArrayList<>();
		soapAction.add("\"\"");
		headers.put("SOAPAction", soapAction);
	}
	


}
