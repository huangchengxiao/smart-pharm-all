package com.visense.smart.pharm.pa.common;

import lombok.Data;

//     返回的说明书结果数据，XML格式和JSON格式
//     * <?xml version="1.0" ?>
//     * <ui_results_xml fun_id="1007">
//     *     <dt_name>药品名称</dt_name>
//     *     <alias name="药品别名">
//     *         <!--药品别名节点, 可循环-->
//     *         <name>药品别名</name>
//     *     </alias>
//     *     <similar name="其他同类药品说明书">
//     *         <!--其他同类药品说明书, 可循环-->
//     *         <medicine>
//     *             <name>药品名称</name>
//     *             <dt_code>药品代码</dt_code>
//     *         </medicine>
//     *     </similiar>
//     *     <instruction>
//     *         <!--说明书内容, 可循环-->
//     *         <info default="默认选中0/1">
//     *             <title color="">标题名称</title>
//     *             <content>说明书内容</content>
//     *         </info>
//     *     </instruction>
//     * </ui_results_xml>
//     *
//     * {
//     *    "Id":"1003", "Result":1, "DrugName":"药品名称", "Alias":[ "药品别名", "药品别名" ], "Classify":"药理分类" "Medicines":
//     *    [
//     *        { "Name":"药品名称", "DrugCode":"药品代码" }
//     *    ], "Inserts":
//     *    [
//     *        { "Selected":"默认选中0/1", "Title":"标题名称", "Color":"标题颜色", "Content":"说明书内容" }
//     *    ]
//     * }
//
@Data
public class InsertMedicine
{
	private String name;
	private String drugCode;

}