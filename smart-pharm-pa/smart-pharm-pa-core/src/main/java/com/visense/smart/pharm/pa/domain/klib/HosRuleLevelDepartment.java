package com.visense.smart.pharm.pa.domain.klib;

import lombok.Data;

/**
 * @author huangchengxiao
 * @date 2024/3/12 15:31
 */
@Data
public class HosRuleLevelDepartment {

    private Integer ID;
    private String DepartmentCode;
    private Integer LevelInfoID;
    private Integer HospCode;
    private Integer HospFlag;
    private Integer LevelID;
    private Integer Deleted;

}
