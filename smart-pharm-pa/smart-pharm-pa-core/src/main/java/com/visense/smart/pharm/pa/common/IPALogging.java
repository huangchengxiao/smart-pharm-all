package com.visense.smart.pharm.pa.common;

public interface IPALogging
{
	void Fatal(Class t, String msg);
	void Error(Class t, String msg);
	void Warn(Class t, String msg);
	void Info(Class t, String msg);
	void Debug(Class t, String msg);
}