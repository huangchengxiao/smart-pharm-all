package com.visense.smart.pharm.pa.mapper.pa;

import com.visense.smart.pharm.mybatis.core.mapper.RootMapper;
import com.visense.smart.pharm.pa.domain.HosInstructionUpLoad;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @author huangchengxiao
 * @date 2024/3/11 9:26
 */
public interface HosInstructionUpLoadMapper extends RootMapper<HosInstructionUpLoad> {
    @Select("select COUNT(1) ZS from Hos_InstructionUpLoad (nolock) where IsDelete=0 and HospCode=#{hospCode} and HosDrugCode=#{hosDrugCode}")
    Integer selectCountByHospCodeAndHosDrugCode(@Param("hospCode") Integer hospCode,@Param("hosDrugCode")  String hosDrugCode);


}
