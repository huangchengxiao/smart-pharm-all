package com.visense.smart.pharm.pa.bean.ns;


import com.visense.smart.pharm.pa.bean.base.PABaseMessage;
import com.visense.smart.pharm.pa.bean.base.PADetailMessage;
import com.visense.smart.pharm.pa.bean.base.PAFunctionId;
import com.visense.smart.pharm.pa.common.IPALogging;
import com.visense.smart.pharm.pa.common.PAHISResultMessage;
import com.visense.smart.pharm.pa.common.PAUIResultMessage;
import com.visense.smart.pharm.pa.service.init.NSManager;
import lombok.Data;

/**
 * PA传入、传出的参数
 * 这么设计是为了保证PA声明的统一性及兼容性
 */
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//[JsonObject(MemberSerialization.OptOut)]
@Data
public abstract class IPAInfo {
    PAFunctionId FunId;

    PABaseMessage BaseMsg;

    PADetailMessage DetailMsg;

    String DetailData;
    String UIResultData;
    String HISResultData;

    IPALogging Logging;
    PAUIResultMessage UIResultMsg;
    PAHISResultMessage HISResultMsg;
    //XmlDocument UIResultsXmlDoc ;
//XmlDocument HISResultsXmlDoc ;
    NSManager NSManager = null;
    int OrigHospCode; // 原始医院代码（知识库保存的）
    int HospCode; // 医院代码（知识库保存的）
    String Id;


}