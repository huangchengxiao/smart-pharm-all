package com.visense.smart.pharm.pa.web.config;

import com.visense.smart.pharm.web.swagger.config.PharmSwaggerAutoConfiguration;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * infra 模块的 web 组件的 Configuration
 *
 * @author huangchengxiao
 */
@Configuration(proxyBeanMethods = false)
public class PaSwaggerConfiguration {

    /**
     * infra 模块的 API 分组
     */
    @Bean
    public GroupedOpenApi infraGroupedOpenApi() {
        return PharmSwaggerAutoConfiguration.buildGroupedOpenApi("", "pa");
    }

}
