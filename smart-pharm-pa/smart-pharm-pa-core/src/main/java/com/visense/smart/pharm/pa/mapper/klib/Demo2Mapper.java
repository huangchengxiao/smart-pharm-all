package com.visense.smart.pharm.pa.mapper.klib;

import org.apache.ibatis.annotations.Select;

public interface Demo2Mapper {
    @Select("select name from DIC_Allergy limit 1;")
    String selectName();
}
