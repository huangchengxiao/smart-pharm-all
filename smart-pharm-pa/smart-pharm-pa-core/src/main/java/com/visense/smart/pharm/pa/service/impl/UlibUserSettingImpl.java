package com.visense.smart.pharm.pa.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.visense.smart.pharm.mybatis.datasource.annotation.DataSource;
import com.visense.smart.pharm.mybatis.enums.DataSourceType;
import com.visense.smart.pharm.pa.domain.ulib.HosInfo;
import com.visense.smart.pharm.pa.mapper.ulib.HosInfoMapper;
import com.visense.smart.pharm.pa.mapper.ulib.SettingMapper;
import com.visense.smart.pharm.pa.service.ULIBUserSetting;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 * @author huangchengxiao
 * @date 2024/3/13 16:58
 */
@Service
public class UlibUserSettingImpl implements ULIBUserSetting {
    @Resource
    SettingMapper settingMapper;
    @Resource
    HosInfoMapper hosInfoMapper;

    // 缓存用户配置
    private static HashMap<String, Integer> settings = new HashMap<String, Integer>();

    /**
     * 取用户配置
     */
    @DataSource(DataSourceType.ULIB)
    public  int GetUserSetting(int hospCode, String code, int defValue) {
        try {
            String key = hospCode + "," + code;
            int value = 0;
            // 读取缓存
            if (Objects.nonNull((value = settings.get(key)))) {
                return value;
            }
            value = -1;
            String sql;
            List<Integer> settingList = settingMapper.selectValueByHospCodeAndFuncCode(hospCode, code);
            if (CollectionUtil.isNotEmpty(settingList)) {
                value = settingList.get(0);
                if (settingList.get(0) > 0 && hospCode != 0) {
                    settings.put(key, value);
                }
            }

            return value == -1 ? defValue : value;
        } catch (Exception e) {
            return defValue;
        }
    }

    /*public static String GetAreaCode(String defValue)
    {
        try
        {
            DataTable dtHospInfo = GetHospInfo();
            if (dtHospInfo == null || dtHospInfo.Rows == null || dtHospInfo.Rows.size() <= 0)
            {
                return defValue;
            }

            String RetValue = dtHospInfo.Rows[0]["HospArea"].toString();

            return DotNetToJavaStringHelper.isNullOrEmpty(RetValue) ? defValue : RetValue;
        }
        catch(Exception e)
        {
            return defValue;
        }
    }*/
    @DataSource(DataSourceType.ULIB)
    public  HosInfo GetHospInfo()
    {
        return hosInfoMapper.selectAll().stream().findFirst().orElse(null);
    }
}
