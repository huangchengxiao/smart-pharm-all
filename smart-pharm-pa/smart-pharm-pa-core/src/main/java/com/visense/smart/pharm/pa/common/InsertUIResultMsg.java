package com.visense.smart.pharm.pa.common;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class InsertUIResultMsg extends PAUIResultMessage
{
	private String drugName;
	private String[] alias;
	private String classify;
	private List<InsertMedicine> medicines;
	private List<InsertContent> inserts;


	public InsertUIResultMsg()
	{
		medicines = new ArrayList<InsertMedicine>();
		inserts = new ArrayList<InsertContent>();
	}
}