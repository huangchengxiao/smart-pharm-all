package com.visense.smart.pharm.pa.common;

import lombok.Data;

@Data
public class InsertContent
{
	private int selected;
	private String title;
	private String color;
	private String content;


}