package com.visense.smart.pharm.pa.mapper.klib;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @author huangchengxiao
 * @date 2024/3/14 11:28
 */
public interface DICInstructionInfoMapper {
    @Select("SELECT COUNT(1) from DIC_InstructionInfoEx where InstructionID=#{InstructionID}")
    Integer selectCountByInstructionID(@Param("InstructionID") int InstructionID);
}
