package com.visense.smart.pharm.pa.service.init;


import cn.hutool.extra.spring.SpringUtil;
import com.visense.smart.pharm.pa.bean.ns.dbAccess.CacheModel;
import com.visense.smart.pharm.pa.bean.ns.dbAccess.SpecialModel;
import com.visense.smart.pharm.pa.bean.ns.dbAccess.SysRuleLevelInfo;
import com.visense.smart.pharm.pa.domain.klib.HosRuleLevelDepartment;
import com.visense.smart.pharm.pa.mapper.klib.HosRuleLevelDepartmentMapper;
import com.visense.smart.pharm.pa.mapper.klib.SysRuleLevelInfoMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class SysCache {

    private static HosRuleLevelDepartmentMapper hosRuleLevelDepartmentMapper = SpringUtil.getBean(HosRuleLevelDepartmentMapper.class);
    private static SysRuleLevelInfoMapper sysRuleLevelInfoMapper = SpringUtil.getBean(SysRuleLevelInfoMapper.class);

    private static Set<CacheModel> HashCache = new HashSet<>();
    private static List<HosRuleLevelDepartment> deptRule = null;

    public static void InitCache() {
        try {
            InitDeptRule();
//C# TO JAVA CONVERTER NOTE: The following 'using' block is replaced by its Java equivalent:
//			using (DbHelper helper = PAAccessModule.PADBAccess.GetDB().Clone())
//            DbHelper helper = PAAccessModule.PADBAccess.GetDB().clone();
            try {
            /*    helper.Open();
                SysLevel = SysRuleLevelInfo.Select(helper, null);*/
                List<SysRuleLevelInfo> SysLevel = sysRuleLevelInfoMapper.selectList();
//C# TO JAVA CONVERTER TODO TASK: There is no equivalent to implicit typing in Java:
                for (SysRuleLevelInfo Item : SysLevel) {
                    //DeptCode = GetDepartmentInfo(Item.ID, 1),
                    CacheModel cacheModule = new CacheModel();
                    cacheModule.setID(Item.ID);
                    cacheModule.setItemCodeA(StringUtils.defaultString(Item.ItemCodeA));
                    cacheModule.setItemCodeB(StringUtils.defaultString(Item.ItemCodeB));
                    cacheModule.setBaseCodeA(Item.BaseCodeA);
                    cacheModule.setBaseCodeB(Item.BaseCodeB);
                    cacheModule.setRuleType(Item.RuleType);
                    cacheModule.setSourceType(Item.SourceType);
                    cacheModule.setLevelID(Item.LevelID);
                    cacheModule.setRuleExt(Item.RuleExt);
                    cacheModule.setRuleExt1(Item.RuleExt1);
                    cacheModule.setSpecials(GetSpecialLevelInfo(Item.ID, 1));
                    cacheModule.setIsDefault(false);
                    HashCache.add(cacheModule);
                }
            } finally {
                //这边是关闭 db 相关逻辑java 统一处理掉
//                helper.dispose();
            }
        } catch (RuntimeException ex) {
            //TODO PALogging 相关逻辑后面再处理
         /*   PALogging.Error(SysCache.class, ex.getMessage());

            if (ex.getCause() != null) {
                PALogging.Error(SysCache.class, ex.getCause().getMessage());
            }*/
        }
    }

    public static void InitDeptRule() {
        if (deptRule != null) {
            return;
        }
        //这块主要是去 padb 查找deptRule相关信息
        List<HosRuleLevelDepartment> hosRuleLevelDepartments = hosRuleLevelDepartmentMapper.selectCache();
        deptRule = hosRuleLevelDepartments;
    }

    public static CacheModel GetCache(CacheModel model) {
        return null;
       /* try {
            Iterable<CacheModel> resultModel = CacheExists(model); //获取所有药匹配项

            if (!DotNetToJavaStringHelper.isNullOrEmpty(model.getRuleExt1())) {
                if (resultModel != null && resultModel.Count() > 0) {
                    if (resultModel.Count() > 1) {
                        //再次查询诊断匹配项
//C# TO JAVA CONVERTER TODO TASK: There is no Java equivalent to LINQ queries:
//C# TO JAVA CONVERTER TODO TASK: Lambda expressions and anonymous methods are not converted by C# to Java Converter:
                        Iterable<CacheModel> resultHasModel = resultModel.Where(item = > !DotNetToJavaStringHelper.isNullOrEmpty(item.RuleExt1) &&
                        item.RuleExt1.equals(model.getRuleExt1()))
                        ;
                        if (resultHasModel != null && resultHasModel.Count() > 0) {
                            return resultHasModel.FirstOrDefault();
                        }
                    } return resultModel.FirstOrDefault();
                }
            } else {
                if (resultModel != null && resultModel.Count() > 0) {
                    return resultModel.FirstOrDefault();
                }
            }

            return GetDefaultCache(model);
        } catch (RuntimeException ex) {
            PALogging.Error(SysCache.class, ex.getMessage());
            if (ex.getCause() != null) {
                PALogging.Error(SysCache.class, ex.getCause().getMessage());
            }
            return null;
        }*/
    }

    public static boolean UpdateCache(int RuleLevelID) {
        //TODO 这块是更新缓存的逻辑
        return true;
       /* SysRuleLevelInfo SysRule = new SysRuleLevelInfo();
        try {
            if (RuleLevelID > 0) {
//C# TO JAVA CONVERTER NOTE: The following 'using' block is replaced by its Java equivalent:
//				using (DbHelper helper = PAAccessModule.PADBAccess.GetDB().Clone())
                DbHelper helper = PAAccessModule.PADBAccess.GetDB().clone();
                try {
                    helper.Open();

                    String[] keys = {"ID"};
                    SysRule = SysRuleLevelInfo.Select(helper, keys, RuleLevelID).FirstOrDefault();

                    synchronized (HashCache) {
                        if (SysRule == null) {
//C# TO JAVA CONVERTER TODO TASK: Lambda expressions and anonymous methods are not converted by C# to Java Converter:
                            HashCache.RemoveWhere(Item = > Item.ID.equals(RuleLevelID));
                        } else {
//C# TO JAVA CONVERTER TODO TASK: There is no equivalent to implicit typing in Java:
//C# TO JAVA CONVERTER TODO TASK: There is no Java equivalent to LINQ queries:
//C# TO JAVA CONVERTER TODO TASK: Lambda expressions and anonymous methods are not converted by C# to Java Converter:
                            var tmpCache = HashCache.Where(Item = > Item.ItemCodeA.equals(SysRule.ItemCodeA == null ? "" : SysRule.ItemCodeA) &&
                            Item.ItemCodeB.equals(SysRule.ItemCodeB == null ? "" : SysRule.ItemCodeB) && Item.RuleType.equals(SysRule.RuleType) &&
                            (DotNetToJavaStringHelper.isNullOrEmpty(SysRule.RuleExt1) || Item.RuleExt1.equals(SysRule.RuleExt1)) &&
                            (DotNetToJavaStringHelper.isNullOrEmpty(SysRule.RuleExt) || Item.RuleExt.equals(SysRule.RuleExt)))
                            ;
                            if (tmpCache.Count() > 0) {
//C# TO JAVA CONVERTER TODO TASK: There is no equivalent to implicit typing in Java:
                                for (var Item : tmpCache) {
                                    Item.LevelID = SysRule.LevelID;
                                    Item.ID = SysRule.ID;
                                    Item.SourceType = 1;
                                    Item.RuleExt1 = SysRule.RuleExt1;
                                    //Item.DeptCode = GetDepartmentInfo(SysRule.ID);
                                    Item.Specials = GetSpecialLevelInfo(SysRule.ID);
                                    Item.IsDefault = false;
                                }
                            } else {
                                //DeptCode = GetDepartmentInfo(SysRule.ID),
                                CacheModel tempVar = new CacheModel();
                                tempVar.setID(SysRule.ID);
                                tempVar.setLevelID(SysRule.LevelID);
                                tempVar.setItemCodeA(SysRule.ItemCodeA == null ? "" : SysRule.ItemCodeA);
                                tempVar.setItemCodeB(SysRule.ItemCodeB == null ? "" : SysRule.ItemCodeB);
                                tempVar.setBaseCodeA(SysRule.BaseCodeA);
                                tempVar.setBaseCodeB(SysRule.BaseCodeB);
                                tempVar.setRuleType(SysRule.RuleType);
                                tempVar.setRuleExt(SysRule.RuleExt);
                                tempVar.setRuleExt1(SysRule.RuleExt1 == null ? "" : SysRule.RuleExt1);
                                tempVar.setSourceType(SysRule.SourceType);
                                tempVar.setSpecials(GetSpecialLevelInfo(SysRule.ID));
                                tempVar.setIsDefault(false);
                                HashCache.add(tempVar);
                            }
                        }
                    }
                } finally {
                    helper.dispose();
                }
            }
        } catch (RuntimeException ex) {
            PALogging.Error(SysCache.class, ex.getMessage());
            if (ex.getCause() != null) {
                PALogging.Error(SysCache.class, ex.getCause().getMessage());
                return false;
            }
        } return true;*/
    }

    private static Iterable<CacheModel> CacheExists(CacheModel model) {
        Iterable<CacheModel> ResultModel = null;
        //TODO 这是判断缓存是否存在的逻辑 是否做这层待定
        return ResultModel;
/*
        if (model.getItemCodeB().length() > 0) {
//C# TO JAVA CONVERTER TODO TASK: There is no Java equivalent to LINQ queries:
//C# TO JAVA CONVERTER TODO TASK: Lambda expressions and anonymous methods are not converted by C# to Java Converter:
            ResultModel = HashCache.Where(Item = > Item.ItemCodeA.equals(model.getItemCodeA()) && Item.ItemCodeB.equals(model.getItemCodeB()) &&
            Item.RuleType.equals(model.getRuleType()) && Item.RuleExt1.equals(model.getRuleExt1()) && (DotNetToJavaStringHelper.isNullOrEmpty(Item
            .RuleExt) || Item.RuleExt.equals(model.getRuleExt())))
            ;

            if (ResultModel == null || ResultModel.Count() == 0) {
//C# TO JAVA CONVERTER TODO TASK: There is no Java equivalent to LINQ queries:
//C# TO JAVA CONVERTER TODO TASK: Lambda expressions and anonymous methods are not converted by C# to Java Converter:
                ResultModel = HashCache.Where(Item = > Item.ItemCodeA.equals(model.getItemCodeA()) && Item.ItemCodeB.equals(model.getItemCodeB())
                && Item.RuleType.equals(model.getRuleType()) && DotNetToJavaStringHelper.isNullOrEmpty(Item.RuleExt1) && (DotNetToJavaStringHelper
                .isNullOrEmpty(Item.RuleExt) || Item.RuleExt.equals(model.getRuleExt())))
                ;
            }

            if (ResultModel == null || ResultModel.Count() == 0) {
//C# TO JAVA CONVERTER TODO TASK: There is no Java equivalent to LINQ queries:
//C# TO JAVA CONVERTER TODO TASK: Lambda expressions and anonymous methods are not converted by C# to Java Converter:
                ResultModel = HashCache.Where(Item = > Item.ItemCodeA.equals(model.getItemCodeB()) && Item.ItemCodeB.equals(model.getItemCodeA())
                && Item.RuleType.equals(model.getRuleType()) && Item.RuleExt1.equals(model.getRuleExt1()) && (DotNetToJavaStringHelper
                .isNullOrEmpty(Item.RuleExt) || Item.RuleExt.equals(model.getRuleExt())))
                ;
            }

            if (ResultModel == null || ResultModel.Count() == 0) {
//C# TO JAVA CONVERTER TODO TASK: There is no Java equivalent to LINQ queries:
//C# TO JAVA CONVERTER TODO TASK: Lambda expressions and anonymous methods are not converted by C# to Java Converter:
                ResultModel = HashCache.Where(Item = > Item.ItemCodeA.equals(model.getItemCodeB()) && Item.ItemCodeB.equals(model.getItemCodeA())
                && Item.RuleType.equals(model.getRuleType()) && DotNetToJavaStringHelper.isNullOrEmpty(Item.RuleExt1) && (DotNetToJavaStringHelper
                .isNullOrEmpty(Item.RuleExt) || Item.RuleExt.equals(model.getRuleExt())))
                ;
            }
        } else {
//C# TO JAVA CONVERTER TODO TASK: There is no Java equivalent to LINQ queries:
//C# TO JAVA CONVERTER TODO TASK: Lambda expressions and anonymous methods are not converted by C# to Java Converter:
            ResultModel = HashCache.Where(Item = > Item.ItemCodeA.equals(model.getItemCodeA()) && Item.RuleType.equals(model.getRuleType()) && Item
            .RuleExt1.equals(model.getRuleExt1()) && (DotNetToJavaStringHelper.isNullOrEmpty(Item.RuleExt) || Item.RuleExt.equals(model.getRuleExt
            ())))
            ;

            if (ResultModel == null || ResultModel.Count() == 0) {
//C# TO JAVA CONVERTER TODO TASK: There is no Java equivalent to LINQ queries:
//C# TO JAVA CONVERTER TODO TASK: Lambda expressions and anonymous methods are not converted by C# to Java Converter:
                ResultModel = HashCache.Where(Item = > Item.ItemCodeA.equals(model.getItemCodeA()) && Item.RuleType.equals(model.getRuleType()) &&
                DotNetToJavaStringHelper.isNullOrEmpty(Item.RuleExt1) && (DotNetToJavaStringHelper.isNullOrEmpty(Item.RuleExt) || Item.RuleExt
                .equals(model.getRuleExt())))
                ;
            }
        }

        if (ResultModel == null || ResultModel.Count() == 0) {
            if (model.getRuleType() == 29 || model.getRuleType() == 20 || model.getRuleType() == 21 || model.getRuleType() == 22 || model
            .getRuleType() == 23 || model.getRuleType() == 38) // IssueType.SBFSJW -  IssueType.BXLHYYECYLHYY -  IssueType.LYHJRYWZLZY -  IssueType
            .LYHJZYWBLFY -  IssueType.CSJKZYDYWLHSY -  IssueType.XHZYWT
            {
//C# TO JAVA CONVERTER TODO TASK: There is no Java equivalent to LINQ queries:
//C# TO JAVA CONVERTER TODO TASK: Lambda expressions and anonymous methods are not converted by C# to Java Converter:
                ResultModel = HashCache.Where(Item = > Item.BaseCodeA.equals(model.getBaseCodeA()) && Item.BaseCodeB.equals(model.getBaseCodeB())
                && Item.RuleType.equals(model.getRuleType()) && (DotNetToJavaStringHelper.isNullOrEmpty(Item.RuleExt1) || Item.RuleExt1.equals
                (model.getRuleExt1())) && (DotNetToJavaStringHelper.isNullOrEmpty(Item.RuleExt) || Item.RuleExt.equals(model.getRuleExt())))
                ;
                if (ResultModel == null || ResultModel.Count() == 0) {
//C# TO JAVA CONVERTER TODO TASK: There is no Java equivalent to LINQ queries:
//C# TO JAVA CONVERTER TODO TASK: Lambda expressions and anonymous methods are not converted by C# to Java Converter:
                    ResultModel = HashCache.Where(Item = > Item.BaseCodeA.equals(model.getBaseCodeB()) && Item.BaseCodeB.equals(model.getBaseCodeA
                    ()) && Item.RuleType.equals(model.getRuleType()) && (DotNetToJavaStringHelper.isNullOrEmpty(Item.RuleExt1) || Item.RuleExt1
                    .equals(model.getRuleExt1())) && (DotNetToJavaStringHelper.isNullOrEmpty(Item.RuleExt) || Item.RuleExt.equals(model.getRuleExt
                    ())))
                    ;
                }
            }
        } if (ResultModel == null || ResultModel.Count() == 0) {
//C# TO JAVA CONVERTER TODO TASK: There is no Java equivalent to LINQ queries:
//C# TO JAVA CONVERTER TODO TASK: Lambda expressions and anonymous methods are not converted by C# to Java Converter:
            ResultModel = HashCache.Where(Item = > (DotNetToJavaStringHelper.isNullOrEmpty(Item.ItemCodeA)) && Item.RuleType.equals(model
            .getRuleType()) && (DotNetToJavaStringHelper.isNullOrEmpty(Item.RuleExt1) || Item.RuleExt1.equals(model.getRuleExt1())) &&
            (DotNetToJavaStringHelper.isNullOrEmpty(Item.RuleExt) || Item.RuleExt.equals(model.getRuleExt()))).
            OrderByDescending(x = > x.ID);
        }

        return ResultModel;*/
    }

    private static CacheModel GetDefaultCache(CacheModel model) {
        CacheModel LevelModel = null;
        return LevelModel;
/*
        try {
//C# TO JAVA CONVERTER TODO TASK: There is no Java equivalent to LINQ queries:
//C# TO JAVA CONVERTER TODO TASK: Lambda expressions and anonymous methods are not converted by C# to Java Converter:
            LevelModel = HashCache.Where(Item = > Item.RuleType.equals(model.getRuleType()) && Item.SourceType.equals(0) &&
            (DotNetToJavaStringHelper.isNullOrEmpty(Item.RuleExt) || Item.RuleExt.equals(model.getRuleExt())) && Item.RuleExt1.equals(model
            .getRuleExt1())).
            FirstOrDefault();

            if (LevelModel == null) {
//C# TO JAVA CONVERTER TODO TASK: There is no Java equivalent to LINQ queries:
//C# TO JAVA CONVERTER TODO TASK: Lambda expressions and anonymous methods are not converted by C# to Java Converter:
                LevelModel = HashCache.Where(Item = > Item.RuleType.equals(model.getRuleType()) && Item.SourceType.equals(0) &&
                (DotNetToJavaStringHelper.isNullOrEmpty(Item.RuleExt) || Item.RuleExt.equals(model.getRuleExt())) && DotNetToJavaStringHelper
                .isNullOrEmpty(Item.RuleExt1)).
                FirstOrDefault();

            }

            if (LevelModel != null) {
                model.setLevelID(LevelModel.getLevelID());
                model.setID(LevelModel.getID());
                model.setSourceType(0);
                model.setIsDefault(true);
                model.setSpecials(LevelModel.getSpecials());
            } else {
                model.setSourceType(0);
                model.setLevelID(2); //将默认级别调整为2级
                model.setID(0);
                model.setIsDefault(true);
            }

            return model;
        } catch (RuntimeException ex) {
            *//*PALogging.Error(SysCache.class, ex.getMessage());
            if (ex.getCause() != null) {
                PALogging.Error(SysCache.class, ex.getCause().getMessage());
            }*//*
            return null;
        }*/
    }

    //C# TO JAVA CONVERTER TODO TASK: C# optional parameters are not converted to Java:
//ORIGINAL LINE: private static List<SpecialModel> GetSpecialLevelInfo(int ID, int sourceType = 0)
    private static List<SpecialModel> GetSpecialLevelInfo(int ID, int sourceType) {
        //TODO 特殊的处理 这层逻辑暂时先不处理
        return null;
       /* List<SpecialModel> model = new List<SpecialModel>();

        if (sourceType == 1) {
            if (deptRule != null && deptRule.size() > 0) {
                DataRowView[] rows = deptRule.FindRows((int) ID);

                if (rows != null && rows.length > 0) {
                    for (DataRowView deptItem : rows) {
                        DataRow DR = (DataRow) deptItem.Row;
                        SpecialModel a = new SpecialModel();
                        List<Integer> hospFlag = new List<Integer>();
                        a.setHospCode(Integer.parseInt(DR["HospCode"].toString()));
                        int dtHospFlag = Integer.parseInt(DR["HospFlag"].toString());
                        if (dtHospFlag == 0) {
                            hospFlag.add(1);
                            hospFlag.add(2);
                            hospFlag.add(4);
                        } else if (dtHospFlag == 1) {
                            hospFlag.add(1);
                        } else if (dtHospFlag == 2) {
                            hospFlag.add(2);
                        } else if (dtHospFlag == 3) {
                            hospFlag.add(1);
                            hospFlag.add(2);
                        } else if (dtHospFlag == 4) {
                            hospFlag.add(4);
                        } else if (dtHospFlag == 5) {
                            hospFlag.add(1);
                            hospFlag.add(4);
                        } else if (dtHospFlag == 6) {
                            hospFlag.add(2);
                            hospFlag.add(4);
                        }
                        a.setHospFlag(hospFlag);
                        a.setLevel(Integer.parseInt(DR["LevelID"].toString()));
                        a.setDeptCode(DR["DepartmentCode"].toString());
                        a.setID(Integer.parseInt(DR["RuleID"].toString()));
                        model.add(a);
                    }

                }
            }
        } else {
//C# TO JAVA CONVERTER NOTE: The following 'using' block is replaced by its Java equivalent:
//			using (DbHelper helper = PAAccessModule.PADBAccess.GetDB().Clone())
            DbHelper helper = PAAccessModule.PADBAccess.GetDB().clone();
            try {
                String sql = "SELECT HospCode,HospFlag,DepartmentCode,LevelID,ID as RuleID FROM Hos_RuleLevelDepartment (nolock)  WHERE
                LevelInfoID=@ID and Deleted=0 order by RuleID";

                java.util.List<DbParameter> parameters = new List<DbParameter>();
                DbParameter parameter = helper.getProviderFactory().CreateParameter();
                parameter.ParameterName = "@ID";
                parameter.DbType = DbType.String;
                parameter.setValue(ID);
                parameters.add(parameter);

                helper.Open();
                DataTable dt = helper.ExecuteQueryWithParameters(sql, parameters);
                if (dt.Rows.size() > 0) {
                    for (DataRow dr : dt.Rows) {
                        SpecialModel a = new SpecialModel();
                        List<Integer> hospFlag = new List<Integer>();
                        a.setHospCode(Integer.parseInt(dr["HospCode"].toString()));
                        if (dr["HospFlag"].toString().equals("0")) {
                            hospFlag.add(1);
                            hospFlag.add(2);
                            hospFlag.add(4);
                        } else if (dr["HospFlag"].toString().equals("1")) {
                            hospFlag.add(1);
                        } else if (dr["HospFlag"].toString().equals("2")) {
                            hospFlag.add(2);
                        } else if (dr["HospFlag"].toString().equals("3")) {
                            hospFlag.add(1);
                            hospFlag.add(2);
                        } else if (dr["HospFlag"].toString().equals("4")) {
                            hospFlag.add(4);
                        } else if (dr["HospFlag"].toString().equals("5")) {
                            hospFlag.add(1);
                            hospFlag.add(4);
                        } else if (dr["HospFlag"].toString().equals("6")) {
                            hospFlag.add(2);
                            hospFlag.add(4);
                        }
                        a.setHospFlag(hospFlag);
                        a.setLevel(Integer.parseInt(dr["LevelID"].toString()));
                        a.setDeptCode(dr["DepartmentCode"].toString());
                        a.setID(Integer.parseInt(dr["RuleID"].toString()));
                        model.add(a);
                    }
                }
            } finally {
                helper.dispose();
            }
        }

        return model;*/
    }

    //C# TO JAVA CONVERTER TODO TASK: C# optional parameters are not converted to Java:
//ORIGINAL LINE: private static List<string> GetDepartmentInfo(int ID, int sourceType = 0)
    private static List<String> GetDepartmentInfo(int ID, int sourceType) {
       /* List<String> deptCode = new List<String>();

        if (sourceType == 1) {
            if (deptRule != null && deptRule.size() > 0) {
                DataRowView[] rows = deptRule.FindRows((int) ID);

                if (rows != null && rows.length > 0) {
                    for (DataRowView row : rows) {
                        deptCode.add(row["DepartmentCode"].toString());
                    }
                }
            }
        } else {
//C# TO JAVA CONVERTER NOTE: The following 'using' block is replaced by its Java equivalent:
//			using (DbHelper helper = PAAccessModule.PADBAccess.GetDB().Clone())
            DbHelper helper = PAAccessModule.PADBAccess.GetDB().clone();
            try {
                String sql = "SELECT DepartmentCode,LevelInfoID FROM Hos_RuleLevelDepartment (nolock) WHERE LevelInfoID=@ID";

                java.util.List<DbParameter> parameters = new List<DbParameter>();
                DbParameter parameter = helper.getProviderFactory().CreateParameter();
                parameter.ParameterName = "@ID";
                parameter.DbType = DbType.String;
                parameter.setValue(ID);
                parameters.add(parameter);

                helper.Open();
                DataTable dt = helper.ExecuteQueryWithParameters(sql, parameters);
                if (dt.Rows.size() > 0) {
                    for (DataRow dr : dt.Rows) {
                        deptCode.add(dr["DepartmentCode"].toString());
                    }
                }
            } finally {
                helper.dispose();
            }
        }

        return deptCode;*/
        return null;
    }
}