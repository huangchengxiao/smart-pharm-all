package com.visense.smart.pharm.pa.common;


public class NSLogging implements IPALogging
{
	private String id;

	public NSLogging(String id)
	{
		this.id = id;
	}

	public final void Fatal(Class t, String msg)
	{
		PALogging.Fatal(t, String.format("%1$s %2$s", id, msg));
	}

	public final void Error(Class t, String msg)
	{
		PALogging.Error(t, String.format("%1$s %2$s", id, msg));
	}

	public final void Warn(Class t, String msg)
	{
		PALogging.Warn(t, String.format("%1$s %2$s", id, msg));
	}

	public final void Info(Class t, String msg)
	{
		PALogging.Info(t, String.format("%1$s %2$s", id, msg));
	}

	public final void Debug(Class t, String msg)
	{
		PALogging.Debug(t, String.format("%1$s %2$s", id, msg));
	}
}