package com.visense.smart.pharm.pa.service;

/**
 * @author huangchengxiao
 * @date 2024/3/14 9:23
 */
public interface DrugInfoService {
      int GetCode(int hospCode, String hisCode, String hospFlag, boolean confirmedOnly);
      String GetInsertName(int code);
}
