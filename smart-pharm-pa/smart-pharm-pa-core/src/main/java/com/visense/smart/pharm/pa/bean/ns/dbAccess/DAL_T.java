package com.visense.smart.pharm.pa.bean.ns.dbAccess;

//C# TO JAVA CONVERTER TODO TASK: The C# 'new()' constraint has no equivalent in Java:
public class DAL_T<T>
{
	/*// DALFactory.Init中反射table_info，所以需要定义为protected
	protected static DBTableInfo table_info = new DBTableInfo();

	private DbHelper privatehelper;
	public  DbHelper gethelper()
	{
		return privatehelper;
	}
	public  void sethelper(DbHelper value)
	{
		privatehelper = value;
	}

	public final Object getItem(String field)
	{
		return table_info.getItem(field).GetValue(this);
	}
	public final void setItem(String field, Object value)
	{
		table_info.getItem(field).SetValue(this, value);
	}

	public static String gettable_name()
	{
		return table_info.table_name;
	}

	public static java.util.List<String> getall_fields_name()
	{
		java.util.List<String> fields_name = new List<String>();

		for (DBColumnAttribute attribute : table_info.column_array)
		{
			fields_name.add(attribute.column_name);
		}

		return fields_name;
	}

	public static java.util.List<T> Select(DbHelper helper, String[] fields_where, Object... values_where)
	{
		java.util.List<String> pk_types = new List<String>();
		java.util.List<DbParameter> parameters = new List<DbParameter>();

		if (fields_where != null)
		{
			for (int i = 0; i < fields_where.length; i++)
			{
				for (DBColumnAttribute attribute : table_info.column_array)
				{
					if (attribute.column_name.equals(fields_where[i]))
					{
						pk_types.add(table_info.field_array.get(attribute.column_name).FieldType.getName());
						break;
					}
				}
			}
		}

		String sql = DbHelper.MakeSelectSQL(gettable_name(), getall_fields_name(), fields_where, pk_types, values_where, parameters);
		DataTable table = helper.ExecuteQueryWithParameters(sql, parameters);

		return MapToObject(table);
	}


	public final boolean Fetch() // 根据PK获取其他信息
	{
		java.util.List<String> pk_fields = new List<String>();
		java.util.List<String> pk_types = new List<String>();
		java.util.List<Object> pk_values = new List<Object>();
		java.util.List<DbParameter> parameters = new List<DbParameter>();

		for (DBColumnAttribute attribute : table_info.column_array)
		{
			if (attribute.IsPrimaryKey())
			{
				pk_fields.add(attribute.column_name);
				pk_types.add(table_info.field_array.get(attribute.column_name).FieldType.getName());
				pk_values.add(this.getItem(attribute.column_name));
			}
		}

		String sql = DbHelper.MakeSelectSQL(gettable_name(), getall_fields_name(), pk_fields, pk_types, pk_values, parameters);

		DataTable table = gethelper().ExecuteQueryWithParameters(sql, parameters);
		if (table.Rows.size() == 0)
		{
			return false;
		}

		if (table.Rows.size() > 1)
		{
			throw new RuntimeException("多条重复数据" + sql);
		}

		return Fetch(table.Rows[0], false);
	}

	public static java.util.List<T> MapToObject(DataTable table)
	{
		java.util.List<T> list = new List<T>();

		for (DataRow row : table.Rows)
		{
			T dal = new T();
			dal.Fetch(row, true);
			list.add(dal);
		}

		return list;
	}

	private boolean Fetch(DataRow row, boolean fetch_pk)
	{
		for (DBColumnAttribute attribute : table_info.column_array)
		{
			try
			{
				if (attribute.IsPrimaryKey() && !fetch_pk)
				{
					continue;
				}

				if ((row[attribute.column_name] != null) && (row[attribute.column_name] != DBNull.getValue()))
				{
				this.setItem(attribute.column_name, row[attribute.column_name]);
				}
			}
			catch(java.lang.Exception e)
			{
				continue;
			}
		}

		return true;
	}

	public final int Insert() // 插入数据库
	{
		java.util.List<String> all_fields = new List<String>();
		java.util.List<String> all_types = new List<String>();
		java.util.List<Object> all_values = new List<Object>();
		java.util.List<DbParameter> parameters = new List<DbParameter>();

		for (DBColumnAttribute attribute : table_info.column_array)
		{
			if(attribute.IsBySystem()) // 由系统控制值
			{
				continue;
			}

			all_fields.add(attribute.column_name);
			all_types.add(table_info.field_array.get(attribute.column_name).FieldType.getName());
			all_values.add(this.getItem(attribute.column_name));
		}

		String sql = DbHelper.MakeInsertSQL(gettable_name(), all_fields, all_types, all_values, parameters);

		String result = gethelper().ExecuteScalar(sql, parameters).toString();

		if (DotNetToJavaStringHelper.isNullOrEmpty(result))
		{
			return 0;
		}

		return Integer.parseInt(result);
		//return helper.ExecuteNonQuery(sql);
	}

	public final int Insert(boolean throw_exception) // 插入数据库
	{
		try
		{
			return Insert();
		}
		catch(java.lang.Exception e)
		{
			if(throw_exception)
			{
				throw e;
			}

			return 0;
		}
	}

	public final int Update() // 根据PK修改
	{
		java.util.List<String> npk_fields = new List<String>();
		java.util.List<Object> npk_values = new List<Object>();

		java.util.List<String> pk_fields = new List<String>();
		java.util.List<String> pk_types = new List<String>();
		java.util.List<Object> pk_values = new List<Object>();
		java.util.List<DbParameter> parameters = new List<DbParameter>();

		for (DBColumnAttribute attribute : table_info.column_array)
		{
			if (attribute.IsPrimaryKey())
			{
				pk_fields.add(attribute.column_name);
				pk_types.add(table_info.field_array.get(attribute.column_name).FieldType.getName());
				pk_values.add(this.getItem(attribute.column_name));
			}
			else if(attribute.IsBySystem() == false) // 非系统控制值
			{
				npk_fields.add(attribute.column_name);
				npk_values.add(this.getItem(attribute.column_name));
			}
		}

		String sql = DbHelper.MakeUpdateSQL(gettable_name(), npk_fields, npk_values, pk_fields, pk_types, pk_values, parameters);

		return gethelper().ExecuteNonQueryWithParameters(sql, parameters);
	}

	public final int Delete() // 根据PK删除
	{
		java.util.List<String> pk_fields = new List<String>();
		java.util.List<String> pk_types = new List<String>();
		java.util.List<Object> pk_values = new List<Object>();
		java.util.List<DbParameter> parameters = new List<DbParameter>();

		for (DBColumnAttribute attribute : table_info.column_array)
		{
			//增加主键的判断
			if (attribute.IsPrimaryKey())
			{
				pk_fields.add(attribute.column_name);
				pk_types.add(table_info.field_array.get(attribute.column_name).FieldType.getName());
				pk_values.add(this.getItem(attribute.column_name));
			}
		}

		String sql = DbHelper.MakeDeleteSQL(gettable_name(), pk_fields, pk_types, pk_values, parameters);
		return gethelper().ExecuteNonQueryWithParameters(sql, parameters);
	}
	public final int Delete(String[] ColumnNames) //根据列名删除
	{
		java.util.List<String> fields = new List<String>();
		java.util.List<String> types = new List<String>();
		java.util.List<Object> values = new List<Object>();
		java.util.List<DbParameter> parameters = new List<DbParameter>();

		for (String ColumnName : ColumnNames)
		{
			fields.add(ColumnName);
			for (DBColumnAttribute attribute : table_info.column_array)
			{
				if (attribute.column_name.equals(ColumnName))
				{
					types.add(table_info.field_array.get(attribute.column_name).FieldType.getName());
					values.add(this.getItem(attribute.column_name));
					break;
				}
			}
		}
		String sql = DbHelper.MakeDeleteSQL(gettable_name(), fields, types, values, parameters);
		return gethelper().ExecuteNonQueryWithParameters(sql, parameters);
	}*/
}