package com.visense.smart.pharm.mybatis.datasource.bean;

/**
 * @author huangchengxiao
 * @date 2024/3/14 14:11
 */
public interface DruidConfigPre {
    String prefix = "spring.datasource.druid";


    String MASTER_NAME = "master";
    String SLAVE_NAME = "slave";
    String KLIB_NAME = "klib";
    String ULIB_NAME = "ulib";

    static String getFull(String name, String suffix) {
        return prefix + "."+name+"." + suffix;
    }

}
