package com.visense.smart.pharm.pa.bean.ns;


import com.visense.smart.pharm.pa.bean.base.PAFunctionId;

public interface INSModule extends INSModuleBase
{
	/** 构件实现的功能号
	 如 return new FunctionId[] { FunctionId.*** };
	*/
	PAFunctionId[] GetFunIds();

	/** 构件实现的功能
	*/
	int Call(IPAInfo info);
}